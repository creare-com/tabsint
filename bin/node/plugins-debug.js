#!/usr/bin/env node

'use strict';

var config = require('./util/config.js');
var plugins = require('./util/plugins.js');
var _ = require('lodash');

if (require.main === module) {
  var c = config.load();

  // iterate through each plugin
  _.forEach(c.plugins, function(p, name) {
    c.plugins[name].debug = true;
  });

  config.write(c);
}
