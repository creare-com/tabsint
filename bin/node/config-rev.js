#!/usr/bin/env node

'use strict';

var config = require('./util/config.js');
var cmd = require('./util/cmd.js');
var log = require('./util/log.js');

/**
 * Write the current rev hash to the config file under the rev key
 */
if (require.main === module) {

  var ver = cmd.run('git describe --always');
  ver = ver.replace('\n','').replace('\t', '');

  var c = config.load();
  c.rev = ver;
  config.write(c);
  log.debug('Wrote rev ' + ver + ' to src/tabsintConfig.json');
}
