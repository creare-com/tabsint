
/**
 * Plugin handling module
 * @module
 */
var plugins = module.exports = {};

// module depedencies
var log = require('./log.js'); // note that when you "require" paths are relative to this specific file
var cmd = require('./cmd.js');
var fs = require('fs-extra');
var process = require('process');
var _ = require('lodash');

/**
 * Write import.js file in src/tabsint_plugins
 */
plugins.writeImportJs = function() {

  // get list of installed plugins
  var entries = fs.readdirSync('src/tabsint_plugins');

  var import_list = _.map(entries, function (entry){
    if (entry !== 'import.js') {
      return '"./' + entry + '/' + entry + '"';
    }
  });

  var module_list = _.map(entries, function (entry){ 
    if (entry !== 'import.js') {
      return '"' + entry + '"';
    }
  });

  // write file string
  var importjs = '"use strict";\n\nimport * as angular from "angular"\n';
  import_list.forEach(function(entry) {
    if (entry) {
      importjs += 'import ' + entry + ';\n';
    }
  });

  importjs += '\n\n' +
    'angular.module("tabsint.plugin-modules", ['  + module_list + ']);\n';

  log.debug('Writing import.js');
  fs.writeFileSync('src/tabsint_plugins/import.js', importjs);
};


/**
 * Get the git version of the plugin repository
 * @param {object} p - plugin object from config file
 * @return {string} git version from 'git describe --always'
 */
plugins.getVersion = function(p) {
  var cwd = process.cwd();   // get the current working directory
  process.chdir(p.src);
  var ver = cmd.run('git describe --always');
  ver = ver.replace('\n','').replace('\t', '');
  process.chdir(cwd);

  return ver;
};

/**
 * Checkout the specified version/tag of the plugin using git
 * @param {object} p - plugin object from config file
 */
plugins.checkout = function(p) {
  var cwd = process.cwd();   // get the current working directory
  
  // checkout using the `src` directory or the `cordova` directory, whatever is defined
  if (p.src) {
    process.chdir(p.src);
  } else if (p.cordova && p.cordova.src) {
    process.chdir(p.cordova.src);
  } else {
    log.error('No plugin directories specified');
    process.exit(2);
  }
  
  var output = cmd.run('git checkout tags/' + p.version);
  log.debug(output);

  process.chdir(cwd);
};

/**
 * Install app plugin
 * @param {object} p - plugin object from config file
 */
plugins.installApp = function(p) {

  // make sure plugins directory exists
  fs.ensureDirSync('src/tabsint_plugins');

  // copy plugin
  try {
    fs.copySync(p.src, 'src/tabsint_plugins/' + p.name);
  } catch(e) {
    log.error('Failed to copy app plugin: ' + p.name, e);
    process.exit(2);
  }
};
