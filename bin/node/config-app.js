#!/usr/bin/env node

"use strict";

var config = require("./util/config.js");
var log = require("./util/log.js");
var fs = require("fs-extra");
var xml2js = require("xml2js");

/**
 * Write the app name, id, and description to the config.xml
 */
if (require.main === module) {
  // load build config
  var c = config.load();

  var parser = new xml2js.Parser();
  var builder = new xml2js.Builder();

  // load config.xml
  parser.parseString(fs.readFileSync("config.xml"), function(err, configxml) {
    if (err) {
      log.error("Error parsing config.xml", err);
    } else {
      var changed = false;

      // if id, name, or desc have changed, updated config.xml
      if (c.appid && !(configxml["widget"]["$"]["id"] == c.appid)) {
        configxml["widget"]["$"]["id"] = c.appid;
        changed = true;
      }
      if (c.appname && !(configxml["widget"]["name"] == c.appname)) {
        configxml["widget"]["name"] = c.appname;
        changed = true;
      }
      if (c.appdesc && !(configxml["widget"]["description"] == c.appdesc)) {
        configxml["widget"]["description"] = c.appdesc;
        changed = true;
      }

      if (changed) {
        log.debug("Updating config.xml.");
        fs.writeFileSync("config.xml", builder.buildObject(configxml));
      }
    }
  });
}
