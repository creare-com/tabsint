/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

import "./tabsint-content/tabsint-content";
import "./exam/exam";
import "./welcome/welcome";
import "./admin/admin";

angular.module("tabsint.routes", [
  "tabsint.routes.tabsint-content",
  "tabsint.routes.welcome",
  "tabsint.routes.exam",
  "tabsint.routes.admin"
]);
