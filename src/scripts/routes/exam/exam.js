/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.routes.exam", [])

  .directive("examView", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/routes/exam/exam.html",
      controller: "ExamCtrl",
      scope: {}
    };
  })

  .controller("ExamCtrl", function(
    $sce,
    $scope,
    adminLogic,
    autoConfig,
    config,
    devices,
    disk,
    examLogic,
    logger,
    page,
    pm,
    remote,
    tabsintNative,
    tabsintReceiver
  ) {
    $scope.pm = pm;
    $scope.admin = adminLogic;
    $scope.debugMode = disk.debugMode;
    $scope.adminSkipMode = disk.adminSkipMode;
    $scope.link = remote.link;
    $scope.isString = angular.isString;
    $scope.config = config;

    // exam logic actions
    $scope.dm = examLogic.dm;
    $scope.page = page;
    $scope.examLogic = examLogic;

    let usbEventCallback = function(connected) {
      logger.debug("examCtrl.usbEventCallback invoked, connected: " + JSON.stringify(connected));
      pm.root.protocolUsbCMissing = !connected;
    };

    tabsintReceiver.handleExternalAppData(autoConfig.androidIntentCallback); //registering androidIntentCallback in exam route

    if (pm.root.headset === "EPHD1") {
      pm.root.protocolUsbCMissing = !tabsintNative.isUsbConnected;

      // USB device listener registration
      tabsintNative.registerUsbDeviceListener(usbEventCallback);
    }

    this.$onDestroy = function() {
      tabsintNative.unregisterUsbDeviceListener(usbEventCallback);
    };

    // trust all input as html
    $scope.trustAsHtml = $sce.trustAsHtml;

    if (devices.platform.toLowerCase() == "browser") {
      $scope.examLogic.begin(); //HG so it it routes straight to EXAM
    }

    // handle instructions class
    $scope.centerIfShort = function(id) {
      if (
        document.getElementById(id) &&
        document.getElementById(id).offsetWidth > 0.8 * document.documentElement.clientWidth
      ) {
        return { "text-align": "left" };
      }
    };

    $scope.$watch("config.build", function() {
      $scope.config = config;
    });
  });
