/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";

import "../../app";

/* global tabsint */

beforeEach(angular.mock.module("tabsint"));

describe("CHA Exams", function() {
  var chaResults, chaExams, page, results, $rootScope;
  beforeEach(
    angular.mock.inject(function(_chaExams_, _chaResults_, _page_, _results_, _$rootScope_) {
      _results_.current = { testResults: { responses: [] } };
      _page_.result = {};
      chaExams = _chaExams_;
      chaResults = _chaResults_;
      results = _results_;
      $rootScope = _$rootScope_;
    })
  );

  it("Dynamic start level should change Lstart for HughsonWestLake", function() {
    let examProperties = {
      DynamicStartLevel: { offset: 12, baseIdList: ["HW_1"] },
      F: 1000,
      Lstart: 30,
      TonePulseNumber: 3,
      UseSoftwareButton: true,
      LevelUnits: "dB HL",
      OutputChannel: "HPR0"
    };
    spyOn(chaResults, "getPastResults").and.returnValue([{ ResultType: "Threshold", Threshold: 40 }]);
    spyOn(chaExams, "setup").and.callThrough();
    chaExams.setup("HughsonWestlake", examProperties);
    expect(chaResults.getPastResults).toHaveBeenCalledWith(["HW_1"]);
    expect(chaExams.examProperties.Lstart).toBe(52);
  });

  it("Dynamic start level should change Lstart for BekesyLike", function() {
    let examProperties = {
      DynamicStartLevel: { offset: 12, baseIdList: ["HW_1"] },
      F: 4000,
      Lstart: 50,
      PresentationMax: 100,
      ToneDuration: 225,
      ToneRamp: 25,
      UseSoftwareButton: true,
      LevelUnits: "dB SPL",
      IncrementStart: 4,
      IncrementNominal: 2,
      OutputChannel: "HPL0"
    };
    spyOn(chaResults, "getPastResults").and.returnValue([{ ResultType: "Threshold", Threshold: 40 }]);
    spyOn(chaExams, "setup").and.callThrough();
    chaExams.setup("BekesyLike", examProperties);
    expect(chaResults.getPastResults).toHaveBeenCalledWith(["HW_1"]);
    expect(chaExams.examProperties.Lstart).toBe(52);
  });
});
