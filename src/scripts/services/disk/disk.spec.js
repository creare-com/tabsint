/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Disk", function() {
  var disk;

  beforeEach(
    angular.mock.inject(function($injector, _disk_) {
      disk = _disk_;
    })
  );

  it("should have some defaults defined.", function() {
    expect(disk.debugMode).toBeFalsy();
    expect(disk.appDeveloperMode).toBeFalsy();
    expect(disk.autoUpload).toBeFalsy();
    expect(disk.gitlab.useTagsOnly).toBeTruthy();
  });
});
