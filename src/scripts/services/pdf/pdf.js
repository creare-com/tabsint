/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import pdfMake from "pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

angular
  .module("tabsint.services.pdf", [])
  .factory("pdfService", function($q, app, disk, gettextCatalog, logger, tabsintFS) {
    var api = {
      createPdf: undefined
    };

    api.createPdf = function(docDefinition, filename) {
      return $q(function(resolve, reject) {
        const pdfDocGenerator = pdfMake.createPdf(docDefinition);
        const dir = "tabsint-pdfs/" + filename;
        if (!app.tablet) {
          resolve(pdfDocGenerator.open());
        } else {
          resolve(
            pdfDocGenerator.getBlob(blob => {
              return tabsintFS
                .createFile(disk.extStorageRootDir, dir, "application/pdf")
                .then(uri => {
                  return blob.arrayBuffer().then(blob_arr => {
                    function arrayBufferToBase64(buffer) {
                      let binary = "";
                      const bytes = new Uint8Array(buffer);
                      const len = bytes.byteLength;
                      for (let i = 0; i < len; i++) {
                        binary += String.fromCharCode(bytes[i]);
                      }
                      return window.btoa(binary);
                    }

                    let blob_str_b64 = arrayBufferToBase64(blob_arr);

                    return tabsintFS.writeBytesFile(uri, blob_str_b64);
                  });
                })
                .then(function() {
                  console.log("Pdf created");
                })
                .catch(function(e) {
                  logger.error("Failed to save pdf to file with error: " + angular.toJson(e));
                  notifications.alert(
                    gettextCatalog.getString("Failed to save pdf to file. Please file an issue at") +
                      " https://gitlab.com/creare-com/tabsint"
                  );
                  return reject("Failed to create pdf with error: " + e);
                });
            })
          );
        }
      });
    };
    return api;
  });
