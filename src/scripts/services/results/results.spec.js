/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

// jshint ignore: start

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import "angular-mocks";
import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Results Service", function() {
  var results, disk, file, sqLite, config, json, paths, pm, networkModel, adminLogic, $scope, $window;

  beforeEach(
    angular.mock.inject(function(
      $injector,
      _$window_,
      _results_,
      _disk_,
      _file_,
      _sqLite_,
      _config_,
      _json_,
      _paths_,
      _pm_,
      _networkModel_,
      _adminLogic_,
      _$httpBackend_
    ) {
      results = _results_;
      disk = _disk_;
      file = _file_;
      sqLite = _sqLite_;
      config = _config_;
      json = _json_;
      paths = _paths_;
      pm = _pm_;
      networkModel = _networkModel_;
      adminLogic = _adminLogic_;
      $window = _$window_;
      $scope = $injector.get("$rootScope");
      _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");
      pm.root = {};
    })
  );

  $window.resolveLocalFileSystemURL = function() {
    return {};
  };

  spyOn($window, "resolveLocalFileSystemURL").and.returnValue($window.resolveLocalFileSystemURL);

  function fakeResult(siteId, date) {
    return {
      siteId: siteId,
      testResults: { fake: "fake result" },
      testDateTime: date
    };
  }

  describe("results.create", function() {
    it("results properties should match schema properties 1:1", function() {
      // results create method looks for pm.root.publicKey, add structure here
      pm.root = { publicKey: "" };
      var jsonSchema = json.load(paths.www("scripts/services/results/results_schema.json"));
      var schema_keys = Object.keys(jsonSchema.properties).sort();
      results.create();
      expect(Object.keys(results.current).sort()).toEqual(schema_keys.sort());
      expect(Object.keys(results.current.testResults).sort()).toEqual(
        Object.keys(jsonSchema.properties.testResults.properties).sort()
      );
    });
  });

  describe("results.upload", function() {
    var r1 = fakeResult(1, new Date().toJSON());
    it("Test that upload calls sqLite.prepareForUpload", function() {
      // results create method looks for pm.root.publicKey, add structure here
      spyOn(sqLite, "prepareForUpload");
      results.upload(0);
      expect(sqLite.prepareForUpload).toHaveBeenCalled();
    });
    // it("Test that upload results w/ index out of scope fails", function() {
    //   // results create method looks for pm.root.publicKey, add structure here
    //   response = results.upload(1);

    // });
  });

  describe("results.save", function() {
    var r1 = fakeResult(1, new Date().toJSON());
    var r2 = fakeResult(2, new Date("2015-07-01T00:00:00").toJSON()); // results are stored by date, so r1 and r2 cannot have the same date
    it("should properly push 1 result to the queue", function() {
      disk.server = "localServer"; // do this so that results do not get uploaded
      results
        .save(r1)
        .then(results.getResultsForResultsView)
        .finally(function() {
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r1))).toBeTruthy();
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r2))).toBeFalsy();
        });
      $scope.$apply();
    });

    it("should properly push 2 results to the queue", function() {
      results
        .save(r1)
        .then(function() {
          return results.save(r2);
        })
        .then(function() {
          expect(sqLite.numLogs.results).toEqual(2);
        })
        .catch(function() {
          expect(false).toBeTruthy();
        });
      $scope.$apply();
    });

    it("should properly delete first result from the queue", function() {
      results
        .save(r1)
        .then(function() {
          return results.save(r2);
        })
        .then(function() {
          return results.delete(r1.testDateTime);
        })
        .then(results.getResultsForResultsView)
        .finally(function() {
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r1))).toBeFalsy();
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r2))).toBeTruthy();
        });
      $scope.$apply();
    });

    it("should properly delete second result from the queue", function() {
      results
        .save(r1)
        .then(function() {
          return results.save(r2);
        })
        .then(function() {
          return results.delete(r1.testDateTime);
        })
        .then(function() {
          return results.delete(r2.testDateTime);
        })
        .then(results.getResultsForResultsView)
        .finally(function() {
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r1))).toBeFalsy();
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r2))).toBeFalsy();
        });
      $scope.$apply();
    });

    it("should backup results every time", function() {});

    it("should export results if its a localServer", function() {});

    it("should upload results if its a gitlab server", function() {});

    it("should show alert if too many results are queued", function() {});
  });
});
