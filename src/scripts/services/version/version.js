/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular.module("tabsint.services.version", []).factory("version", function($q, $window, file, json, logger, paths) {
  var version = {
    dm: {}
  };

  version.load = function() {
    return file
      .readFile(cordova.file.applicationDirectory + "www/version.json")
      .then(function(res) {
        var deferred = $q.defer();
        version.dm = res;
        logger.param.version = version.dm.tabsint;
        logger.info("Version object processed: " + angular.toJson(version.dm));
        deferred.resolve(version);
        return deferred.promise;
      })
      .catch(function(e) {
        logger.error("Version failed during load with error: " + angular.toJson(e));
      });
  };

  return version;
});
