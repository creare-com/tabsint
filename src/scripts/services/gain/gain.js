/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular.module("tabsint.services.gain", []).factory("gain", function(devices, disk) {
  /*
       Loads in devices for access to devices.model, which determines the gain to apply to wav files
       */
  var gain = {
    reset: undefined
  };

  // Note: these gain values are relative to that of the Nexus7.
  var tabletGainsNexus7 = {
    Browser: -1,
    "Nexus 7": 0,
    SamsungTabE: -8.5868,
    EPHD1: 0,
    WAHTS: 0,
    SamsungTabA: -4.0596,
    SamsungTabA7Lite: -4.05
  };

  // Note: these gain values are relative to that of the TabE.

  var tabletGainsTabE = {
    Browser: -1,
    "Nexus 7": 8.5868,
    SamsungTabE: 0,
    EPHD1: 0,
    WAHTS: 0,
    SamsungTabA: 4.5272,
    SamsungTabA7Lite: 4.5368
  };

  /**
   * Load the gain factor on app load
   * Only utilized the first time tabsint is loaded
   */
  //TODO: Remove since gain determined per-media file?
  gain.load = function() {
    if (!disk.tabletGain) {
      gain.reset();
    }
  };
  /**
   * Returns tablet gain for this device to compensate for sound card differences.
   * For legacy calibrated protocols, always use Nexus 7 map for backwards compatibility.
   * For protocols calibrated after TabSINT 4.1.0 release date, use TabE map.
   *
   * @param {*} calibration
   */
  gain.getTabletGain = function(calibration) {
    let tabletGain, gainMap;

    if (angular.isDefined(calibration.tablet) && calibration.tablet == "TabE") {
      gainMap = tabletGainsTabE;
    } else {
      gainMap = tabletGainsNexus7;
    }
    if (disk.headset === "EPHD1") {
      tabletGain = gainMap.EPHD1;
    } else if (disk.headset === "Creare Headset" || disk.headset === "WAHTS") {
      tabletGain = gainMap.WAHTS;
    } else if (Object.keys(gainMap).indexOf(devices.model) > -1) {
      tabletGain = gainMap[devices.model];
    } else if (devices.model === "SAMSUNG-SM-T377A") {
      tabletGain = gainMap.SamsungTabE;
    } else if (devices.model === "SM-T380") {
      tabletGain = gainMap.SamsungTabA;
    } else if (devices.model === "SM-T220") {
      tabletGain = gainMap.SamsungTabA7Lite;
    } else {
      tabletGain = gainMap["Nexus 7"];
    }
    return tabletGain;
  };

  /**
   * Reset the custom gain parameter for the current tablet
   */
  gain.reset = function() {
    // disk.tabletGain is not used when protocol specified media to be played as-recorded
    disk.tabletGain = undefined;
    if (disk.headset === "EPHD1") {
      disk.tabletGain = tabletGainsTabE.EPHD1;
    } else if (Object.keys(tabletGainsTabE).indexOf(devices.model) > -1) {
      disk.tabletGain = tabletGainsTabE[devices.model];
    } else if (devices.model === "SAMSUNG-SM-T377A") {
      disk.tabletGain = tabletGainsTabE.SamsungTabE;
    } else if (devices.model === "SM-T380") {
      disk.tabletGain = tabletGainsTabE.SamsungTabA;
    } else if (devices.model === "SM-T220") {
      disk.tabletGain = tabletGainsTabE.SamsungTabA7Lite;
    } else {
      disk.tabletGain = tabletGainsTabE.SamsungTabE;
    }
  };

  return gain;
});
