/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

angular
  .module("tabsint.services.cha.results", [])
  .factory("chaResults", function(results, page, chaCheck, logger, gettextCatalog) {
    // convenience function to splice substrings out of a string
    // s: string - the string that may contain substrings to be removed
    // rem: array[str,str,str,...] - array of strings to remove from s if present
    // returns: new s, sans substrings in rem
    function removeStrings(s, rem) {
      for (var i = 0; i < rem.length; i++) {
        var r = rem[i];
        var ind = s.indexOf(r);

        if (ind === 0) {
          s = s.slice(ind + r.length);
        } else if (ind > 0 && ind + r.length < s.length) {
          var tmp = s.slice(0, ind);
          tmp += s.slice(ind + r.length);
          s = tmp;
        } else if (ind > 0) {
          s = s.slice(0, ind);
        }
      }
      return s;
    }

    var chaResults = {};

    /**
     * Convenience function to grab specific results from the stack
     * 
     * Note: per ECMA docs, the results will be appended based on ascending index
     * order of the idList.
     * 
     * @param idList array of string - partial or full match presentationIds
              TODO: idList appears to be displayIds, not presentationIds.
     * @param matchFields array of obj of type {
           field: str,
           list[val, val, val]
          }
          field is the field in the result object to match
          list contains the values you are looking
          for
     * @returns array of matched result objects
     */
    chaResults.getPastResults = function(idList, matchFields) {
      // retrieve old results, add current response
      var responses = angular.copy(results.current.testResults.responses);
      if (angular.isDefined(page.result.examType)) {
        responses.push(page.result);
      }

      var resultsList = [];
      if (angular.isDefined(idList) && idList.length > 0) {
        idList.forEach(id => {
          let resultList = responses.filter(response => {
            // var hasExamType = angular.isDefined(response.examType); // if there isn't an examType, it's not a cha exam and shouldn't be tabulated
            //TODO: Clean up this function, forking logic for chaManualAudiometry separately
            // to reduce scope of changes at 4.1.0
            var hasPresentationId;
            if (
              response.responseArea === "chaManualAudiometry" ||
              response.responseArea === "chaManualAudiometryExam"
            ) {
              hasPresentationId = response.presentationId.slice(0, id.length) === id;
            } else if (response.responseArea === "chaManualScreener") {
              hasPresentationId = response.presentationId.indexOf(id) >= 0;
            } else {
              hasPresentationId = response.presentationId === id;
            }
            //if no examType, ignore the response
            if (!angular.isDefined(response.examType)) {
              hasPresentationId = false;
            }
            return hasPresentationId;
          });
          resultsList = resultsList.concat(resultList);
        });
      }

      if (matchFields && matchFields.length > 0) {
        _.forEach(matchFields, function(matchSet) {
          resultsList = _.filter(resultsList, function(res) {
            return res[matchSet.field] && matchSet.list.indexOf(res[matchSet.field]) > -1;
          });
        });
      }
      console.log("resultsList", resultsList);
      return resultsList;
    };

    chaResults.addTOBResults = function(tobResults) {
      if (tobResults.Frequencies === angular.undefined || tobResults.Leq === angular.undefined) {
        tobResults.dataError = "No results returned";
        page.result = $.extend({}, page.result, {
          tobResults: tobResults
        });
      } else {
        // round floating point issues
        var roundedFrequencies = tobResults.Frequencies.map(function(f) {
          return Math.round(f);
        });

        var roundedLevels = tobResults.Leq.map(function(leq) {
          return Math.round(leq * 1000) / 1000;
        });
        // push all results onto the dm
        page.result = $.extend({}, page.result, {
          tobResults: {
            Frequencies: roundedFrequencies,
            Leq: roundedLevels
          }
        });
      }
    };

    chaResults.createAudiometryResults = function(idList) {
      console.log("idList", idList);
      var audiogramData = {
        frequency: [],
        level: [],
        channel: [],
        ResultType: [], // array of Strings
        masking: [], // array of booleans -> true if masking was used
        xLabel: gettextCatalog.getString("Frequency (Hz)"),
        yLabel: gettextCatalog.getString("Hearing Level") + " (" + units + ")",
        title: undefined
      };
      var matchList, units;

      // audiogram passes in idList from protocol, displays at the end of audiometry exams do not - it must be created
      if (angular.isUndefined(idList)) {
        if (
          angular.isDefined(page.dm.responseArea.plotProperties) &&
          angular.isDefined(page.dm.responseArea.plotProperties.displayAudiogram)
        ) {
          var plotProperties = page.dm.responseArea.plotProperties || {};
          idList = plotProperties.displayAudiogram;
        } else if (angular.isDefined(page.dm.responseArea.displayIds)) {
          idList = page.dm.responseArea.displayIds;
        } else {
          idList = [""];
        }
      }

      var resultsList;
      if (angular.isDefined(page.result.examType)) {
        matchList = [
          {
            field: "examType",
            list: [page.result.examType]
          }
        ];
        resultsList = chaResults.getPastResults(idList, matchList);
        units = page.result.Units || gettextCatalog.getString("dB HL");
      } else {
        // we are tabling previous results - only use the idList
        resultsList = chaResults.getPastResults(idList, null);
        if (resultsList.length > 0 && resultsList[0].Units) {
          units = resultsList[0].Units;
        } else {
          units = "dB HL";
        }
      }

      console.log("resultsList", resultsList);
      if (resultsList.length > 0) {
        var used_ids = [];
        var resultsListReverse = resultsList.reverse();
        for (var i = 0; i < resultsListReverse.length; i++) {
          // original
          // pushResult(resultsList[i]);

          // reverse results to eliminate duplicates
          if (!used_ids.includes(resultsListReverse[i].presentationId)) {
            pushResult(resultsListReverse[i]);
            used_ids.push(resultsListReverse[i].presentationId);
            console.log("resultsListReverse[i].presentationId", resultsListReverse[i].presentationId);
          }
        }

        audiogramData.yLabel = gettextCatalog.getString("Hearing Level") + " (" + units + ")";
        audiogramData.xLabel = gettextCatalog.getString("Frequency (Hz)");

        return [resultsList, audiogramData];
      } else {
        return [resultsList, audiogramData];
      }

      function pushResult(result) {
        if (
          angular.isDefined(result) &&
          angular.isDefined(result.examProperties) &&
          angular.isDefined(result.presentationId && angular.isDefined(result.examType))
        ) {
          var F, L, channel, id, passFail, index;
          try {
            channel = result.examProperties.OutputChannel;
            id = result.presentationId;
            index = result.presentationIndex;

            // check left/right/bone
            if (channel === "HPL0" || channel === "HPL1") {
              channel = "left";
            } else if (channel === "HPR0" || channel === "HPR1") {
              channel = "right";
            } else if (channel === "LINEL0 NONE") {
              // bone conduction left
              channel = "bone_left";
            } else if (channel === "NONE LINEL0") {
              // bone conduction right
              channel = "bone_right";
            }

            if (chaCheck.isLevelExam(result.examType)) {
              if (result.examProperties.Screener || result.ResponseType === "pass-fail") {
                F = angular.isNumber(result.examProperties.F) ? result.examProperties.F : "-";
                passFail = result.response ? result.response : result.ResultType;
              } else {
                F = angular.isNumber(result.examProperties.F) ? result.examProperties.F : 1000;
                L = isFinite(result.Threshold) ? result.Threshold : "-";

                if (!_.includes([F, L], "-") && !isNaN(L)) {
                  audiogramData.frequency.push(F);
                  audiogramData.level.push(L);
                  audiogramData.channel.push(channel);
                  audiogramData.ResultType.push(result.ResultType);
                  if (!isNaN(result.MaskingLevel) || !isNaN(result.FinalMaskingLevel)) {
                    audiogramData.masking.push(true);
                  } else {
                    audiogramData.masking.push(false);
                  }
                }
              }
            }
          } catch (err) {
            logger.error("error while trying to create audiometry figure: " + angular.toJson(err));
          }
        }
      }
    };

    chaResults.createGapData = function(gapResults) {
      var gapData = {
        x: undefined,
        y: undefined,
        xLabel: gettextCatalog.getString("Presentation") + " #",
        yLabel: gettextCatalog.getString("Gap Length") + " (ms)",
        title: gettextCatalog.getString("Gap Detection Results")
      };
      var maxY = 0;
      gapResults.GapLengthArray.forEach(function(entry) {
        maxY = entry > maxY ? entry : maxY;
      });
      maxY = maxY === 0 ? 200 : maxY + 10;
      gapData.y = gapResults.GapLengthArray;
      gapData.hit = gapResults.HitOrMissArray;
      gapData.maxY = maxY;
      gapData.GapThreshold = 0;
      if (gapResults.GapThreshold && !gapResults.GapThreshold.isNaN) {
        gapData.GapThreshold = gapResults.GapThreshold;
      }
      gapData.reversals = gapResults.ReversalUsedForThresholdArray;
      return gapData;
    };

    chaResults.createDPOAEData = function(dpoaeResults) {
      var resultsList = []; // TODO not used

      var ret = {
        data: [],
        xLabel: "F2 Frequency, kHz",
        yLabel: "Level, dB SPL", // TODO: should this support dB HL as well?
        title: "DPOAE"
      };
      var plotProperties = page.dm.responseArea.plotProperties || {}; // TODO not used

      // ret.data = dpoaeResults.resultList;
      ret.data = [];
      dpoaeResults.forEach(aResult => {
        ret.data.push(aResult.result);
      });

      return ret;
    };

    chaResults.createDPOAETableData = function(dpoaeResults) {
      var ret = [];
      dpoaeResults.forEach(aResult => {
        ret.push({
          f2: Math.round(aResult.page.responseArea.examProperties.F2 / 100) / 10,
          level: Math.round(aResult.result.DpLow.Amplitude),
          noiseFloor: Math.round(aResult.result.DpLow.NoiseFloor),
          snr: Math.round(aResult.result.DpLow.Amplitude - aResult.result.DpLow.NoiseFloor),
          f1Amp: Math.round(aResult.result.F1.Amplitude),
          f2Amp: Math.round(aResult.result.F2.Amplitude),
          channel: aResult.result.channel
        });
      });
      return ret;
    };

    chaResults.createTOBFigures = function(results, standard) {
      var startFreq = 250 - 1;
      var endFreq = 10080 + 1;
      var tobData = {};

      // One limit set
      var limitSet1 = [
        //{
        //  freq: 125,
        //  limit: 24
        //},
        //{
        //  freq: 250,
        //  limit: 16
        //},
        {
          freq: 500,
          limit: 11
        },
        {
          freq: 800,
          limit: 10
        },
        {
          freq: 1000,
          limit: 8
        },
        {
          freq: 1600,
          limit: 9
        },
        {
          freq: 2000,
          limit: 9
        },
        {
          freq: 3150,
          limit: 8
        },
        {
          freq: 4000,
          limit: 6
        },
        {
          freq: 6300,
          limit: 8
        }
        //{
        //  freq: 8000,
        //  limit: 9
        //}
      ];

      // choose limit set to use
      var limits = limitSet1;

      if (angular.isDefined(standard)) {
        tobData.standard = standard;
      }

      for (var i = 0; i < results.length; i++) {
        var tmp = results[i];
        if (tmp.examProperties.InputChannel === "SMICR1") {
          addData(tmp, "right");
        } else if (tmp.examProperties.InputChannel === "SMICR0") {
          addData(tmp, "left");
        } else if (tmp.examProperties.InputChannel === "DLINE0") {
          // MPLUG --- check to see the startRight parameter
          if (tmp.startRight) {
            addData(tmp, "right");
          } else {
            addData(tmp, "left");
          }
        }
      }

      function addData(data, channel) {
        tobData[channel] = [];
        for (var i = 0; i < data.Frequencies.length; i++) {
          // reduce data set to window of interest
          if (data.Frequencies[i] >= startFreq && data.Frequencies[i] <= endFreq) {
            // handling mapping the frequencies that don't match with the limit frequencies
            var freqMap = Math.round(data.Frequencies[i]);
            var limit = 1000; // default to put unset limits off-secreen
            for (var j = 0; j < limits.length; j++) {
              var freqDiff = Math.abs(freqMap - limits[j].freq) / freqMap;
              if (freqDiff < 0.05) {
                // seems to always be within 2%
                limit = limits[j].limit; // set limit if it exists
              }
            }

            tobData[channel].push({
              freq: freqMap,
              level: data.Leq[i] > 0 ? data.Leq[i] : 0, // min of 0
              limit: limit
            });
          }
        }
      }

      // may want tables some day, keeping same chaResults as createAudiometryResults
      var tables = {};

      return [tables, tobData];
    };

    chaResults.calculateLAeq = function(tob_freqs, tob_levels) {
      /* Function to calculate LAeq from frequencies and levels
      - Comes from: https://code.crearecomputing.com/hearingproducts/hearing-algorithms/-/tree/main/Matlab/TOB_to_LAeq

        Inputs: tob_freqs, tob_levels (arrays of frequencies and levels)
        Outputs: LAeq
      */

      function A_weighting(arr) {
        // E.2.3
        let fr = 1000;
        let fl = 10 ** 1.5;
        let fh = 10 ** 3.9;
        // E.2.4
        let D = Math.sqrt(0.5);
        let b = (1 / (1 - D)) * (fr ** 2 + (fl ** 2 * fh ** 2) / fr ** 2 - D * (fl ** 2 + fh ** 2));
        let c = fl ** 2 * fh ** 2;
        let f1 = Math.sqrt((-b - Math.sqrt(b ** 2 - 4 * c)) / 2);
        let f4 = Math.sqrt((-b + Math.sqrt(b ** 2 - 4 * c)) / 2);
        // E.3.4
        let fa = 10 ** 2.45;
        let f2 = ((3 - Math.sqrt(5)) / 2) * fa;
        let f3 = ((3 + Math.sqrt(5)) / 2) * fa;
        // E.3.1
        let A_dB =
          10 *
            Math.log10(
              ((f4 ** 2 * arr ** 4) /
                ((arr ** 2 + f1 ** 2) *
                  Math.sqrt(arr ** 2 + f2 ** 2) *
                  Math.sqrt(arr ** 2 + f3 ** 2) *
                  (arr ** 2 + f4 ** 2))) **
                2
            ) +
          1.999655535667262;

        return A_dB;
      }

      function calculateREUGCorrections(arr) {
        let cor = [];
        // NOTE: table for the 'REUG' (may change)
        let REUG_table_keys = [-1, 10, 1000, 2500, 6000, 20000, 10000000000000]; //padded with small num and large num
        let REUG_table_values = [0, 0, 0, 15, 0, 0, 0]; // padded with 0 for the small and large padding in the keys above
        for (let i = 0; i < arr.length; i++) {
          for (let j = 1; j < REUG_table_keys.length; j++) {
            // loop through table keys to find the 2 values to interpolate between
            if (arr[i] >= REUG_table_keys[j - 1] && arr[i] < REUG_table_keys[j]) {
              let m = (REUG_table_values[j] - REUG_table_values[j - 1]) / (REUG_table_keys[j] - REUG_table_keys[j - 1]);
              cor.push(m * (arr[i] - REUG_table_keys[j - 1]) + REUG_table_values[j - 1]);
            }
          }
        }
        return cor;
      }

      // Uses linear interpolation to get continuous conversion from the table
      let corrections = calculateREUGCorrections(tob_freqs);
      // generate weights
      let A_weights = tob_freqs.map(e => {
        return A_weighting(e);
      });
      // table conversion REUG
      let TOB_Level_dBSPL_ff = [];
      for (let i = 0; i < tob_levels.length; i++) {
        TOB_Level_dBSPL_ff.push(tob_levels[i] - corrections[i]);
      }
      // add the A weights
      let TOB_Level_dBA_ff = [];
      for (let i = 0; i < TOB_Level_dBSPL_ff.length; i++) {
        TOB_Level_dBA_ff.push(TOB_Level_dBSPL_ff[i] + A_weights[i]);
      }
      // determine power
      let TOB_Power = TOB_Level_dBA_ff.map(e => {
        if (e < 900) {
          return 10 ** (e / 10);
        } else {
          return 0;
        }
      });
      // find total power
      let Total_Power = TOB_Power.reduce((partialSum, a) => partialSum + a, 0);
      // LAeq
      let LAeq = 10 * Math.log10(Total_Power);
      return LAeq;
    };

    chaResults.getDisplayThreshold = function(result) {
      // Manual Audiometry result type is "threshold". Automated is "Threshold". Convert to
      // lowercase to catch both cases
      var threshold;
      //Skipped automated audiometry results will have no ResultType (or a ResultType of 0?) Return empty str.
      if (angular.isUndefined(result.ResultType) || result.ResultType === 0) {
        return "";
      }
      if (["BekesyLike", "BekesyMLD"].includes(result.examType) && typeof result.Threshold === "number") {
        threshold = Math.round(result.Threshold);
      } else {
        threshold = result.Threshold;
      }
      if (result.ResultType.toLowerCase() == "threshold") {
        return threshold;
      } else if (result.ResultType == "Failed to Converge") {
        return "DNC";
      } else if (result.ResultType == "Hearing Potentially Better than Calibrated") {
        return `(-${threshold})`;
      } else if (result.ResultType == "Hearing Potentially Beyond the Calibrated Range") {
        return `(${threshold}+)`;
      }
    };

    return chaResults;
  });
