"use strict";

import * as angular from "angular";
import TabSINTFS from "../../../../../custom_plugins/com.creare.tabsintfs/www/tabsintfs";

angular
  .module("tabsint.services.cordova.tabsintFS", [])

  .factory("tabsintFS", function($q, app, devices, cordova, logger, notifications, disk, gettextCatalog, chaStreaming) {
    var tabsintFS = {
      initialize: undefined,
      getExtStorageUri: undefined,
      checkUriPermission: undefined,
      uriExists: undefined,
      exists: undefined,
      createDirectory: undefined,
      createDirectoryFromUri: undefined,
      createFile: undefined,
      writeStringFile: undefined,
      writeBytesFile: undefined,
      writeUuidFile: undefined,
      readUuidFile: undefined,
      listDocumentFiles: undefined,
      listFiles: undefined,
      listFilesTree: undefined,
      copyExtStorageFiles: undefined,
      getNameFromURI: undefined,
      deleteCopiedInternalDir: undefined,
      getDocumentFileName: undefined
    };

    tabsintFS.initialize = function() {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          return TabSINTFS.initialize();
        } else if (devices.platform.toLowerCase() === "ios") {
          // iOS - do nothing
        } else if (devices.platform.toLowerCase() === "browser") {
          // browser - do nothing
        } else {
          notifications.alert(gettextCatalog.getString("ERROR: TabSINTFS Failed to Initialize"));
          logger.error("TabSINTFS failed to initialize. devices.platform: " + devices.platform);
        }
      });
    };

    tabsintFS.getExtStorageUri = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] getExtStorageUri: " + uriString);
          return TabSINTFS.getExtStorageUri(uriString);
        }
      });
    };

    tabsintFS.checkUriPermission = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] checkUriPermission: " + uriString);
          return TabSINTFS.checkUriPermission(uriString);
        }
      });
    };

    tabsintFS.uriExists = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] uriExists: " + uriString);
          return TabSINTFS.uriExists(uriString);
        }
      });
    };

    tabsintFS.exists = function(uriString, name) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] exists " + uriString + " / " + name);
          return TabSINTFS.exists(uriString, name);
        }
      });
    };

    tabsintFS.createDirectory = function(uriString, name) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] createDirectory " + uriString + " / " + name);
          return TabSINTFS.createDirectory(uriString, name);
        }
      });
    };

    tabsintFS.createDirectoryFromUri = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] createDirectoryFromUri " + uriString);
          return TabSINTFS.createDirectoryFromUri(uriString);
        }
      });
    };

    tabsintFS.createFile = function(uriString, name, mimeType) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] createFile " + uriString + " / " + name + " (" + mimeType + ")");
          return TabSINTFS.createFile(uriString, name, mimeType);
        }
      });
    };

    tabsintFS.writeStringFile = function(uriString, string) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] writeStringFile " + uriString);
          return TabSINTFS.writeStringFile(uriString, string);
        }
      });
    };

    tabsintFS.writeBytesFile = function(uriString, bytesBlob) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] writeBytesFile " + uriString);
          return TabSINTFS.writeBytesFile(uriString, bytesBlob);
        }
      });
    };

    tabsintFS.writeUuidFile = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] writeUuidFile " + uriString);
          return TabSINTFS.writeUuidFile(uriString);
        }
      });
    };

    tabsintFS.readUuidFile = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] readUuidFile " + uriString);
          return TabSINTFS.readUuidFile(uriString);
        }
      });
    };

    tabsintFS.listDocumentFiles = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] listDocumentFiles " + uriString);
          return TabSINTFS.listDocumentFiles(uriString);
        }
      });
    };

    tabsintFS.listFiles = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] listFiles " + uriString);
          return TabSINTFS.listFiles(uriString);
        }
      });
    };

    tabsintFS.listFilesTree = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] listFilesTree " + uriString);
          return TabSINTFS.listFilesTree(uriString);
        }
      });
    };

    tabsintFS.copyExtStorageFiles = function(uriString, dirName) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] copyExtStorageFiles " + uriString);
          return TabSINTFS.copyExtStorageFiles(uriString, dirName);
        }
      });
    };

    tabsintFS.getNameFromURI = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] getNameFromURI " + uriString);
          return TabSINTFS.getNameFromURI(uriString);
        }
      });
    };

    tabsintFS.deleteCopiedInternalDir = function(uriString, dirName) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] deleteCopiedInternalDir " + uriString);
          return TabSINTFS.deleteCopiedInternalDir(uriString, dirName);
        }
      });
    };

    tabsintFS.getDocumentFileName = function(uriString) {
      return cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          console.log("[tabsintFS.js] getDocumentFileName " + uriString);
          return TabSINTFS.getDocumentFileName(uriString);
        }
      });
    };

    return tabsintFS;
  });
