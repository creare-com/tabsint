/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global TabSINTReceiver */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.cordova.tabsintReceiver", [])

  .factory("tabsintReceiver", function($timeout, cordova, devices, gettextCatalog, logger, notifications) {
    var tabsintReceiver = {
      initialize: undefined,
      handleExternalAppData: undefined,
      externalAppDataHandler: undefined,
      sendExternalData: undefined,
      getExternalData: undefined,
      setExternalDataHandler: undefined,
      androidIntentListener: undefined
    };

    tabsintReceiver.initialize = function(successCallback, errorCallback) {
      try {
        return cordova.ready().then(function() {
          if (devices.platform.toLowerCase() === "android") {
            return TabSINTReceiver.initialize(function() {
              TabSINTReceiver.setExternalDataIntentHandler(tabsintReceiver.externalAppDataHandler, function(e) {
                logger.error("tabsint receiver initialize setExternalDataIntentHandler: " + JSON.stringify(e));
              });
              if (successCallback && typeof successCallback === "function") {
                successCallback();
              }
            }, errorCallback);
          } else if (devices.platform.toLowerCase() === "ios") {
            // iOS - do nothing
          } else if (devices.platform.toLowerCase() === "browser") {
            // browser - do nothing
          } else {
            notifications.alert(gettextCatalog.getString("ERROR: TabSINT Receiver Failed to Initialize"));
            logger.error("TabSINT Receiver failed to initialize. devices.platform: " + devices.platform);
          }
        });
      } catch (e) {
        logger.debug("error initializing tabsintReceiver" + e);
      }
    };

    tabsintReceiver.externalAppDataHandler = function(message) {
      logger.debug("tabsintReceiver externalAppDataHandler. Data: " + message["data"]);
      var data = JSON.parse(message["data"]);
      if (data.app == "com.wahtshearing.audhere.testtaker" || data.app == "com.wahtshearing.audhere.allinone") {
        if (angular.isUndefined(tabsintReceiver.androidIntentListener)) {
          // this happens when audhere launches tabsint, it needs time to register callback
          // hold on to data until callback is defined
          // doesn't work because the message would only be accessible in "onCreate", which is only
          // available in the CordovaPlugin
          // $timeout(tabsintReceiver.externalAppDataHandler(message), 1000);
        } else if (
          tabsintReceiver.androidIntentListener &&
          typeof tabsintReceiver.androidIntentListener === "function"
        ) {
          logger.debug("tabsintReceiver externalAppDataHandler firing callback: " + JSON.stringify(data));
          tabsintReceiver.androidIntentListener(data);
        }
      }
    };

    tabsintReceiver.sendExternalData = function(successCallback, errorCallback, externalAppName, data) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          var dataString = JSON.stringify(data);
          TabSINTReceiver.sendExternalData(successCallback, errorCallback, externalAppName, dataString);
        } else if (devices.platform.toLowerCase() === "ios") {
          // iOS - do nothing
        } else if (devices.platform.toLowerCase() === "browser") {
          // browser - do nothing
        } else {
          notifications.alert(gettextCatalog.getString("ERROR: TabSINT Receiver Failed to send external data."));
          logger.error("TabSINT Receiver failed to send external data. devices.platform: " + devices.platform);
        }
      });
    };

    tabsintReceiver.getExternalData = function(successCallback, errorCallback) {
      // This will probably never be needed - getExternalDataIntent is only required if the app will be started from external data.
      // The app is triggered by external data, and the listener is not established yet, so have to use getExternalDataIntent to
      // get that first message...
      cordova.ready().then(function() {
        function handleData(d) {
          try {
            var dataObj = JSON.parse(d.data);
            successCallback(dataObj);
          } catch (e) {
            logger.error(
              "Could not parse incoming data object.  Error: " +
                JSON.stringify(e) +
                ".  The incoming data object should be a JSON.stringified object.  Received: " +
                d
            );
          }
        }

        if (devices.platform.toLowerCase() === "android") {
          TabSINTReceiver.getExternalDataIntent(handleData, errorCallback);
        } else if (devices.platform.toLowerCase() === "ios") {
          // iOS - do nothing
        } else if (devices.platform.toLowerCase() === "browser") {
          // browser - do nothing
        } else {
          notifications.alert(gettextCatalog.getString("ERROR: TabSINT Receiver Failed to Get External Data."));
          logger.error("TabSINT Receiver failed to getExternalData. devices.platform: " + devices.platform);
        }
      });
    };

    // For use when external app is initiating communication
    tabsintReceiver.handleExternalAppData = function(callback) {
      cordova.ready().then(function() {
        if (devices.platform.toLowerCase() === "android") {
          tabsintReceiver.androidIntentListener = callback;
        }
      });
    };

    // For use when external app is started from tabsint
    tabsintReceiver.setExternalDataHandler = function(successCallback, errorCallback) {
      cordova.ready().then(function() {
        function handleData(d) {
          if (d.type && d.type === "text/plain" && d.data) {
            try {
              var dataObj = JSON.parse(d.data);
              successCallback(dataObj);
            } catch (e) {
              logger.error(
                "Could not parse incoming data object.  Error: " +
                  JSON.stringify(e) +
                  ".  The incoming data object should be a JSON.stringified object.  Received: " +
                  JSON.stringify(d)
              );
            }
          }
        }
        if (devices.platform.toLowerCase() === "android") {
          TabSINTReceiver.setExternalDataIntentHandler(handleData, errorCallback);
        } else if (devices.platform.toLowerCase() === "ios") {
          // iOS - do nothing
        } else if (devices.platform.toLowerCase() === "browser") {
          // browser - do nothing
        } else {
          notifications.alert(gettextCatalog.getString("ERROR: TabSINT Receiver Failed to set external data handler."));
          logger.error("TabSINT Receiver failed to setExternalDataHandler. devices.platform: " + devices.platform);
        }
      });
    };
    return tabsintReceiver;
  });
