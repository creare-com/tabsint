/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.page", [])

  .factory("page", function() {
    var page = {};

    // data model for the current page
    page.dm = undefined;

    // data model for the current page
    page.result = undefined;

    return page;
  });
