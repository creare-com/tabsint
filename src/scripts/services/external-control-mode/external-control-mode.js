/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.external-control-mode", [])
  .factory("externalControlMode", function($timeout, pm, page) {
    /*
             Service to enable control of protocol flow using external tools like Matlab
             */
    var externalControlMode = {
      activateExternalPage: undefined,
      getExternalPage: undefined,
      resetExternal: undefined,
      submitExternal: undefined,
      startPageForExternalControl: {
        title: "Matlab Interface",
        subtitle: "This protocol will be controlled externally from Matlab",
        instructionText: "Press begin to start the exam",
        helpText: "none"
      }
    };

    //// TODO figure out if we need this function
    //            externalControlMode.activateExternalPage = function(page){
    //                examLogic.submit = examLogic.submitDefault;
    //                page.dm = undefined;
    //                $rootScope.$apply();
    //                page.result = undefined;
    //                page.dm.canGoBack =  page.dm.enableBackButton || false;
    //                // initialize callback
    //                page.dm.showFeedback = undefined;
    //
    //                // Determine page submission logic.
    //                page.dm.isSubmittable = examLogic.getSubmittableLogic(page.dm.responseArea);
    //
    //                // Create new result.
    //                var result = {
    //                    presentationId: page.dm.id,
    //                    response: undefined,
    //                    correct: undefined
    //                };
    //
    //
    //                $timeout(function() {
    //                    examLogic.finishActivate(page.dm, result);
    //                    examLogic.dm.state.displayMode = 'ENABLED';
    //                },20);
    //            };

    externalControlMode.getExternalPage = function() {
      console.log("EXTERNAL COMMAND:  this is where we ask Matlab to serve up the next page");
    };

    externalControlMode.resetExternal = function(startPage) {
      startPage = startPage || externalControlMode.startPageForExternalControl;

      pm.root = {
        _audioProfileVersion: undefined,
        _calibrationPySVNRevision: undefined,
        _calibrationPyManualReleaseDate: undefined
      };

      return startPage;
    };

    externalControlMode.submitExternal = function() {
      // count it as a question if the answer is not undefined...
      if (page.result.response !== undefined) {
        page.dm.state.iQuestion += 1;
      }

      page.dm.state.displayMode = "DISABLED"; // slight pause disabled, then switch to next page

      // This Section Works With The animate-switch-container and animate-switch.
      // a bit of a hack, using 2 exam pages (exactly the same) and flipping between them to trigger a switch slide
      if (page.dm.showFeedback === undefined) {
        console.log(
          "EXTERNAL COMMAND: page done, with XY results in case they are needed for next page logic, send next page or finalize"
        );
      } else {
        page.dm.showFeedback();
        $timeout(function() {
          console.log(
            "EXTERNAL COMMAND: page done, with XY results in case they are needed for next page logic, send next page or finalize"
          );
        }, 1250);
      }
    };

    return externalControlMode;
  });
