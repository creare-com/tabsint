/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular.module("tabsint.services.flic", []).factory("flic", function(tabsintNative, logger) {
  var flic = {
    state: "ready",
    buttons: [],
    listeners: {},
    registerListener: function(id, downCallback, upCallback) {
      // Check if ID is already used.
      if (id in flic.listeners || (!downCallback && !upCallback)) {
        return false; // Failed to register.
      }
      flic.listeners[id] = {
        downCallback: downCallback,
        upCallback: upCallback
      };
      return true; // Success
    },
    removeListener: function(id) {
      return delete flic.listeners[id];
    },
    scan: function() {
      flic.state = "scanning";
      tabsintNative.scanFlicButton(
        success => {
          var utcDate = Date.now();
          logger.info(
            `Flic button ${success.button} event ${success.event} at ${
              success.timestamp
            }. Current timestamp: ${utcDate}`
          );
          logger.info(`Time difference (java -> this callback): ${utcDate - success.timestamp} milliseconds.`);

          if (!success.success) {
            logger.error("The Flic response is reporting failure.");
            return;
          }
          if (success.event === "addButton") {
            flic.buttons.push({ name: success.button });
          } else {
            Object.keys(flic.listeners).forEach((id, index) => {
              var callback =
                success.event === "isDown"
                  ? flic.listeners[id].downCallback
                  : success.event === "isUp"
                  ? flic.listeners[id].upCallback
                  : undefined;
              if (callback) {
                callback();
              }
            });
          }
        },
        error => {
          logger.error("Flic scan failed with error: ", error);
        }
      );
      flic.state = "ready";
    }
  };
  return flic;
});
