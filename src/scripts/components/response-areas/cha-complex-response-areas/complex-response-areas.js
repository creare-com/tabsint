/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "./complex-response-areas.ctrl";
import "./manual-audiometry/manual-audiometry";
import "./record-file/record-file";
import "./third-octave-bands-extended/third-octave-bands-extended";
import "./manual-audiometry/manual-audiometry-exam";
import "./manual-screener/manual-screener";
import "./manual-tone-generation/manual-tone-generation";
import "./playsound-array/playsound-array";
import "./gap/gap";
import "./calibration-check/calibration-check";

angular.module("tabsint.components.response-areas.cha.complex-response-areas", [
  "cha.complex-response-areas.ctrl",
  "cha.manual-audiometry",
  "cha.record-file",
  "cha.third-octave-bands-extended",
  "cha.manual-audiometry-exam",
  "cha.manual-screener",
  "cha.manual-tone-generation",
  "cha.playsound-array",
  "cha.gap",
  "cha.calibration-check"
]);
