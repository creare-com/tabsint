/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

angular
  .module("cha.third-octave-bands-extended", [])
  .directive("thirdOctaveBandsExtended", function() {
    return {
      restrict: "E",
      templateUrl:
        "scripts/components/response-areas/cha-complex-response-areas/third-octave-bands-extended/third-octave-bands-extended.html",
      controller: "ThirdOctaveBandsExtendedCtrl"
    };
  })
  .controller("ThirdOctaveBandsExtendedCtrl", function($scope, cha, chaExams, chaResults, logger, page, svantek) {
    // TODO: Change the level of the feature parameter so that is it not under examProperties
    var examProperties = JSON.parse(JSON.stringify(page.dm.responseArea.examProperties));
    if (examProperties.feature) {
      $scope.type = examProperties.feature;
    } else {
      $scope.type = "continuous";
    }
    delete examProperties.feature;

    // setup continuous or svantek
    if ($scope.type == "continuous") {
      if (angular.isUndefined(examProperties.BufferLength)) {
        // Set default to about 1 second (just under) if no parameter is specified
        examProperties.BufferLength = 40960;
      }
      if (!angular.isDefined(page.result["list_of_tobs"])) {
        page.result["list_of_tobs"] = [];
      }
    } else if ($scope.type == "svantek") {
      if (angular.isUndefined(examProperties.BufferLength)) {
        // Set default to 4 seconds if no parameter is specified
        examProperties.BufferLength = 41667 * 4;
      }
    } else {
      logger.info("Unsupported feature type for thirdOctaveBandsExtended " + type);
    }

    page.dm.isSubmittable = true;
    $scope.btnState = 0;
    $scope.bufferLengthToTime = 1000.0 / 41667; // units of ms. E.G. - 40960*bufferLengthToTime = just under 1 sec
    $scope.waitTime = parseInt(0.75 * $scope.bufferLengthToTime * examProperties.BufferLength); // 75% of buffer length, units of ms
    $scope.pollInterval = 50;

    $scope.recordButtonClass = function() {
      let presentButtonClass = [
        "btn btn-block btn-record",
        "btn btn-block btn-record active",
        "btn btn-block btn-record disabled"
      ];
      return presentButtonClass[$scope.btnState];
    };

    $scope.toggleRecording = function() {
      if ($scope.btnState === 1) {
        $scope.btnState = 0;
        stopRecording();
      } else {
        startRecording();
      }
    };

    function startRecording() {
      if ($scope.type == "continuous") {
        $scope.btnState = 1;
        runTOB();
      } else if ($scope.type == "svantek") {
        $scope.btnState = 2;
        $scope.svantekMPlugError = undefined;
        runSvantek();
      } else {
        logger.info("Unsupported feature type for thirdOctaveBandsExtended " + type);
      }
    }

    function stopRecording() {
      return cha.abortExams().then(function() {
        logger.info("Exam has been aborted");
        $scope.btnState = 0;
      });
    }

    function runTOB() {
      cha
        .requestStatusBeforeExam()
        .then(() => {
          return cha.queueExam("ThirdOctaveBands", examProperties);
        })
        .then(() => {
          return chaExams.wait.forReadyState($scope.waitTime, $scope.pollInterval);
        })
        .then(() => {
          return getContinuousExamResults();
        })
        .catch(function(err) {
          logger.info("TOB extended exam failed with error: " + JSON.stringify(err));
          cha.errorHandler.main("TOB extended exam failed with error: " + JSON.stringify(err));
        });
    }

    function getContinuousExamResults() {
      if ($scope.btnState == 0) {
        return;
      } else {
        return cha
          .requestResults()
          .then(res => {
            res["time"] = new Date();
            page.result["list_of_tobs"].push(res);
            if ($scope.btnState == 1) {
              return runTOB();
            }
          })
          .catch(() => {
            logger.info("Error in getContinuousExamResults");
          });
      }
    }

    function runSvantek() {
      return cha
        .requestStatusBeforeExam()
        .then(() => {
          return svantek.stop();
        })
        .then(() => {
          return svantek.start();
        })
        .catch(function(e) {
          logger.info("Error stopping or starting svantek");
        })
        .then(() => {
          return cha.queueExam("ThirdOctaveBands", examProperties);
        })
        .then(() => {
          return chaExams.wait.forReadyState($scope.waitTime, $scope.pollInterval);
        })
        .then(() => {
          return svantek.stop();
        })
        .then(() => {
          return cha.requestResults();
        })
        .then(res => {
          return handleSvantekComparisonResults(res);
        })
        .catch(function(err) {
          $scope.svantekMPlugError = true;
          $scope.tableResults = undefined;
          logger.info("ERROR (in catch of runSvantek) " + JSON.stringify(err));
        })
        .finally(() => {
          if ($scope.btnState == 2) {
            $scope.btnState = 0;
          }
        });
    }

    function handleSvantekComparisonResults(resTOB) {
      resTOB["time"] = new Date();
      if (page.result.tob == undefined) {
        page.result.tob = [resTOB];
      } else {
        page.result.tob.push(resTOB);
      }

      // Hardcoded frequencies from MPlug (total of 33)
      let mplug_freqs = [
        12,
        16,
        20,
        25,
        31,
        39,
        50,
        63,
        79,
        99,
        125,
        157,
        198,
        250,
        315,
        397,
        500,
        630,
        794,
        1000,
        1260,
        1587,
        2000,
        2520,
        3175,
        4000,
        5040,
        6350,
        8000,
        10079,
        12699,
        16000,
        20159
      ];
      // Not all frequencies from MPlug are available on Svantek (first 2 and last 3 are missing)
      let svantek_freqs = mplug_freqs.slice(2, mplug_freqs.length - 3);

      if (page.result.svantek == undefined) {
        page.result.svantek = [svantek.data];
      } else {
        page.result.svantek.push(svantek.data);
      }

      $scope.tableResults = [];
      let last_run_mplug_idx = page.result.tob.length - 1;
      let last_run_svantek_idx = page.result.svantek.length - 1;

      function isValueLessThan900(val) {
        if (val < 900) {
          return true;
        } else {
          return false;
        }
      }

      let cnt = 0;
      svantek_freqs.forEach((freq, idx) => {
        if (isValueLessThan900(page.result.tob[last_run_mplug_idx].Leq.slice(2, mplug_freqs.length - 3)[idx])) {
          $scope.tableResults[cnt] = {};
          $scope.tableResults[cnt].f = svantek_freqs[idx];
          $scope.tableResults[cnt].svantekLevel = page.result.svantek[last_run_svantek_idx].Leq[idx];
          $scope.tableResults[cnt].mplugLevel = page.result.tob[last_run_mplug_idx].Leq.slice(
            2,
            mplug_freqs.length - 3
          )[idx];
          $scope.tableResults[cnt].difference =
            $scope.tableResults[cnt].svantekLevel - $scope.tableResults[cnt].mplugLevel;
          cnt += 1;
        }
      });

      let tob_freqs = [];
      let tob_levels_mplug = [];
      let tob_levels_svantek = [];
      for (const [key, value] of Object.entries($scope.tableResults)) {
        tob_freqs.push(value.f);
        tob_levels_mplug.push(value.mplugLevel);
        tob_levels_svantek.push(value.svantekLevel);
      }

      $scope.LAeq_mplug = chaResults.calculateLAeq(tob_freqs, tob_levels_mplug);
      $scope.LAeq_svantek = chaResults.calculateLAeq(tob_freqs, tob_levels_svantek);
      $scope.attenuation_difference = $scope.LAeq_svantek - $scope.LAeq_mplug;
    }
  });
