/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.playsound-array", [])
  .directive("playsoundArray", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/cha-complex-response-areas/playsound-array/playsound-array.html",
      controller: "PlaysoundArrayCtrl"
    };
  })
  .controller("PlaysoundArrayCtrl", function($scope, chaExams, examLogic, logger, page, results) {
    // view fields
    var i;
    $scope.speakerArray = [];
    for (i = 0; i < page.dm.responseArea.nSpeakers; i++) {
      $scope.speakerArray.push({
        wavefileName: "None",
        wavefilePath: "None",
        useMetaRMS: false,
        soundLevel: 72,
        label: "Play"
      });
    }
    $scope.wavefiles = ["None"];
    for (i = 0; i < page.dm.responseArea.chaWavFiles.length; i++) {
      $scope.wavefiles.push(page.dm.responseArea.chaWavFiles[i].soundFileName);
    }

    var startTime = new Date();
    page.dm.isSubmittable = true;
    examLogic.submit = function() {
      submitResults();
    };

    var examType = "PlaysoundArray"; // remove 'cha' from prefix
    chaExams.setup(examType); // sets chaExams.examProperties

    $scope.play = function(speaker) {
      var wavefile = {};
      if ($scope.speakerArray[speaker].label === "Play") {
        $scope.speakerArray[speaker].label = "Pause";
        wavefile.useMetaRMS = $scope.speakerArray[speaker].useMetaRMS;
        wavefile.path = $scope.speakerArray[speaker].wavefilePath;
        wavefile.Leq = [$scope.speakerArray[speaker].soundLevel, $scope.speakerArray[speaker].soundLevel, 0, 0];
        chaExams.playSound([wavefile]);
      } else {
        $scope.speakerArray[speaker].label = "Play";
        // Need a way to stop sound or change label when sound ends
      }
    };

    $scope.playAll = function() {
      // Hardcoded for FieldSINT phase I demonstration. Will need to update logic for long term implementation.
      var wavefile = {};
      wavefile.useMetaRMS = false;
      wavefile.path = $scope.speakerArray[0].wavefilePath;
      wavefile.Leq = [$scope.speakerArray[0].soundLevel, $scope.speakerArray[0].soundLevel, 0, 0];
      chaExams.playSound([wavefile]);
    };

    $scope.changeWavefile = function(speaker, wavefile) {
      var ind = $scope.wavefiles.indexOf(wavefile);
      if (ind === 0) {
        $scope.speakerArray[speaker].wavefileName = "None";
        $scope.speakerArray[speaker].wavefilePath = "None";
      } else {
        $scope.speakerArray[speaker].wavefileName = page.dm.responseArea.chaWavFiles[ind - 1].soundFileName;
        $scope.speakerArray[speaker].useMetaRMS = page.dm.responseArea.chaWavFiles[ind - 1].useMetaRMS;
        $scope.speakerArray[speaker].wavefilePath = page.dm.responseArea.chaWavFiles[ind - 1].path;
      }
    };

    function submitResults() {
      page.result = results.default(page.dm);
      page.result = $.extend({}, page.result, {
        examType: "",
        presentationId: page.dm.id,
        responseStartTime: startTime,
        chaInfo: chaExams.getChaInfo(),
        ResponseType: page.dm.responseArea.type.substring(3),
        response: "complete"
      });

      logger.debug("Pushing playsound array presentation onto stack: " + angular.toJson(page.result));

      // submit results
      examLogic.submit = examLogic.submitDefault;
      examLogic.submit();
    }
  });
