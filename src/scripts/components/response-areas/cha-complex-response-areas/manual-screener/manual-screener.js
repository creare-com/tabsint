/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.manual-screener", [])
  .directive("manualScreenerExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/cha-complex-response-areas/manual-screener/manual-screener.html",
      controller: "ManualScreenerExamCtrl"
    };
  })
  .controller("ManualScreenerExamCtrl", function(
    $q,
    $scope,
    $timeout,
    cha,
    chaExams,
    chaResults,
    chaConstants,
    disk,
    examLogic,
    logger,
    notifications,
    page,
    results
  ) {
    var examType = page.dm.audiometryType || "HughsonWestlake"; // remove 'cha' from prefix
    chaExams.setup(examType, page.dm.responseArea.examProperties); // sets chaExams.examProperties

    $scope.showPresentedTones = page.dm.responseArea.showPresentedTones;
    $scope.presentationList = page.dm.responseArea.presentationList
      ? angular.copy(page.dm.responseArea.presentationList)
      : [
          {
            F: 500
          },
          {
            F: 1000
          },
          {
            F: 2000
          }
        ];
    $scope.levels = page.dm.responseArea.levels ? angular.copy(page.dm.responseArea.levels).sort() : [25, 40, 60];
    // view fields
    $scope.data = {
      currentFrequency: undefined,
      currentLevel: undefined,
      channel: undefined,
      levelUnits: undefined
    };

    $scope.panes = {
      level: true,
      table: true
    };

    var presentationIndex = 0;
    var levelIndex = 0;
    var defaultExamProperties = chaExams.examProperties || {};
    var defaultPresentationId = page.dm.id;
    var startTime = new Date();

    examLogic.submit = function() {
      submitResults();
    };
    page.dm.isSubmittable = true;

    //build presentation list
    if (
      angular.isUndefined(chaExams.storage.manualScreener) ||
      angular.isUndefined(chaExams.storage.manualScreener.list)
    ) {
      chaExams.storage.manualScreener = {
        list: {}
      };
      // Use deep copy to protect deeply-nested objects
      chaExams.storage.manualScreener.list.Left = _.cloneDeep(
        new Array($scope.presentationList.length).fill(0).map(() => new Array($scope.levels.length).fill(0))
      );
      chaExams.storage.manualScreener.list.Right = _.cloneDeep(
        new Array($scope.presentationList.length).fill(0).map(() => new Array($scope.levels.length).fill(0))
      );

      // build exams for left ear
      _.forEach(page.dm.responseArea.presentationList, function(pres, idx) {
        _.forEach($scope.levels, function(lev, ind) {
          var examProps = _.extend(
            {},
            defaultExamProperties,
            pres.examProperties,
            {
              F: pres.F
            },
            {
              Lstart: lev
            },
            {
              PresentationMax: 1
            }
          );
          examProps.OutputChannel = "HPL0";
          if (examProps.id) {
            delete examProps.id;
          }
          pres.examProperties = examProps;
          chaExams.storage.manualScreener.list.Left[idx][ind] = _.cloneDeep(pres);
        });
      });

      // build exams for right ear
      _.forEach(page.dm.responseArea.presentationList, function(pres, idx) {
        _.forEach($scope.levels, function(lev, ind) {
          var examProps = _.extend(
            {},
            defaultExamProperties,
            pres.examProperties,
            {
              F: pres.F
            },
            {
              Lstart: lev
            },
            {
              PresentationMax: 1
            }
          );
          examProps.OutputChannel = "HPR0";
          if (examProps.id) {
            delete examProps.id;
          }
          pres.examProperties = examProps;
          chaExams.storage.manualScreener.list.Right[idx][ind] = _.cloneDeep(pres);
        });
      });
    }

    // set level units
    var levelUnits = defaultExamProperties.LevelUnits;
    if (angular.isUndefined(levelUnits)) {
      logger.warn("no levelUnits defined for chaManualScreener, defaulting to dB HL");
      $scope.data.levelUnits = "dB HL";
    } else {
      $scope.data.levelUnits = levelUnits;
    }

    $scope.chooseFrequency = function(index) {
      stopPresentation(); // stop presentation, if started
      presentationIndex = index;
      $scope.data.currentFrequency = $scope.presentationList[index].F;
    };

    $scope.toggleToneButton = function(earSide, index) {
      levelIndex = index;
      $scope.data.currentLevel = $scope.levels[levelIndex];
      $scope.data.channel = earSide;
      if ($scope.btnState === 1) {
        stopPresentation();
      } else {
        presentTone();
      }
    };

    $scope.setPass = function() {
      stopPresentation(); // stop presentation, if started

      chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].Threshold = "Pass";
      chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].F =
        $scope.data.currentFrequency;
      chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].L =
        $scope.data.currentLevel;

      nextFrequency();
    };

    $scope.setFail = function() {
      stopPresentation(); // stop presentation, if started

      chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].Threshold = "Refer";
      chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].F =
        $scope.data.currentFrequency;
      chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].L =
        $scope.data.currentLevel;

      nextFrequency();
    };

    function nextFrequency() {
      let freqDone = true;
      // See if all thresholds were set for one frequency. If yes, move to next frequency
      _.forEach($scope.levels, function(lev, ind) {
        if (angular.isUndefined(chaExams.storage.manualScreener.list.Left[presentationIndex][ind].Threshold)) {
          freqDone = false;
        }
        if (angular.isUndefined(chaExams.storage.manualScreener.list.Right[presentationIndex][ind].Threshold)) {
          freqDone = false;
        }
      });
      if (freqDone && presentationIndex < $scope.presentationList.length - 1) {
        $scope.chooseFrequency(presentationIndex + 1);
      }
    }

    function stopPresentation() {
      return cha.abortExams().then(function() {
        $scope.btnState = 0;
      });
    }

    function presentTone() {
      $scope.btnState = 1;

      if (
        !angular.isDefined(chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].L)
      ) {
        chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].L = [];
      }
      chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].examProperties.Lstart =
        $scope.data.currentLevel;

      let examProperties =
        chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex][levelIndex].examProperties;

      var waitFunction = function() {
        chaExams.wait
          .forReadyState()
          .then(cha.requestResults()) //try to add a step where we check if things worked out.
          .then(function() {
            if ($scope.btnState === 1) {
              $scope.btnState = 0;
            }
          })
          .catch(function(err) {
            cha.errorHandler.main(err);
          });
      };

      cha
        .requestStatusBeforeExam()
        .then(function() {
          return cha.queueExam("HughsonWestlake", examProperties);
        })
        .then(waitFunction)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    function submitResults() {
      var earSide = "";
      var recordedStartTime = false;

      function submitSingleResult(pres, index) {
        var presId;
        if (angular.isDefined(pres.id)) {
          presId = earSide + "_" + pres.id;
        } else {
          presId =
            defaultPresentationId +
            earSide +
            "_HW_" +
            pres.examProperties.F +
            "HZ_" +
            pres.examProperties.Lstart +
            pres.examProperties.LevelUnits +
            "_" +
            pres.examProperties.OutputChannel[2];
        }

        if (page.dm.responseArea.onlySubmitFrequenciesTested) {
          if (angular.isUndefined(pres.Threshold)) {
            return;
          }
        }

        page.result = results.default(page.dm);
        if (!recordedStartTime) {
          page.result.responseStartTime = startTime;
          recordedStartTime = true;
        }
        page.result = $.extend({}, page.result, {
          examType: examType,
          presentationId: presId,
          chaInfo: chaExams.getChaInfo(),
          ResponseType: "pass-fail",
          presentationIndex: index,
          RetSPL: 0,
          Units: pres.examProperties.LevelUnits,
          L: pres.L,
          F: pres.F,
          response: angular.isDefined(pres.Threshold) ? pres.Threshold : "No Result",
          examProperties: pres.examProperties
        });
        logger.debug("CHA pushing manual screener presentation result onto stack: " + angular.toJson(page.result));
        examLogic.pushResults();
      }

      earSide = "Left";
      _.forEach(chaExams.storage.manualScreener.list.Left, function(freqArray) {
        _.forEach(freqArray, submitSingleResult);
      });
      earSide = "Right";
      _.forEach(chaExams.storage.manualScreener.list.Right, function(freqArray) {
        _.forEach(freqArray, submitSingleResult);
      });

      page.result = {
        presentationId: "manualScreener",
        responseStartTime: startTime,
        response: "complete"
      };

      // clear storage
      delete chaExams.storage.manualScreener;

      // push results
      examLogic.submit = examLogic.submitDefault;
      examLogic.submit();
    }

    // start off in the first frequency on left ear
    $scope.data.channel = "Left";
    $scope.chooseFrequency(presentationIndex);

    $scope.formatScreenerResult = function(result) {
      if (result == "Pass") {
        return "Pass";
      } else if (result == "Refer") {
        return "Refer";
      }
    };

    $scope.getColor = function(result) {
      if (angular.isDefined(result)) {
        if (result.Threshold == "Refer") {
          return "red";
        }
      }
      return "black";
    };

    cha.errorHandler.responseArea = function(err) {
      // Display error to user with popup notifications
      logger.error("CHA - manual-screener error: " + JSON.stringify(err));
      $scope.chaError = true;

      if (err.msg) {
        //special error handling for exceeding maximum calibrated level.
        if (err.code === 10) {
          notifications.alert(
            "The target amplitude at this frequency is outside the calibrated bounds for this headset."
          );
          //reset button state
          $scope.btnState = 0;
          //reset the last entry on the list. No need to store something out of range.
          chaExams.storage.manualScreener.list[$scope.data.channel][presentationIndex].L.pop();
        } else if (err.code !== 1) {
          //ignore code 1, which is an error from trying to ask the WAHTS to do something while it's busy
          notifications.alert(err.msg);
        }
      } else {
        notifications.alert("Internal error, please restart the Manual Screener test.");
      }
    };
  });
