/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.response-areas.subject-id", [])

  .controller("SubjectIdResponseAreaCtrl", function($scope, adminLogic, examLogic, page, results) {
    $scope.gen = page.dm.responseArea.generate;

    $scope.$watch("page.result.response", function() {
      //submission logic
      $scope.page.dm.isSubmittable = examLogic.getSubmittableLogic(page.dm.responseArea);
    });

    $scope.storeResponse = function() {
      results.current.subjectId = page.result.response; // store in the top level exam subject-id field
    };

    $scope.generate = function() {
      // Generate a random n-digit ascii/letter-number
      var len = 5;
      var text = "";
      var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
      for (var i = 0; i < len; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      page.result.response = text; // store in the current response
      $scope.storeResponse();
    };
  });
