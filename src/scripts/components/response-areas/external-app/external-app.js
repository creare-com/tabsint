/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.response-areas.external-app", [])

  .controller("ExternalAppResponseAreaCtrl", function(
    $scope,
    cha,
    disk,
    examLogic,
    gettextCatalog,
    logger,
    media,
    notifications,
    page,
    paths,
    pm,
    tabsintReceiver
  ) {
    $scope.externalApp = {
      dataOut: page.dm.responseArea.dataOut,
      dataIn: "waiting",
      appName: page.dm.responseArea.appName
    };

    // Set the submittable logic to always be true
    page.dm.isSubmittable = true;

    // boiler plate stuff for Unity games
    var dir = disk.extStorageRootDir + "/tabsint-protocols/" + disk.protocol.name + "/filesToCopy/"; //This is for backward compatibility with the CAT protocol. In the future, we should have filesToCopy be configurable from the protocol, OR renamed to filesForUnity
    var noCalibrationWavfiles = [];
    var vol = [];
    var volMasker = [];
    var volSignal = [];
    // var rewards = ["coffeeMaker", "fan", "mug", "pencilHolder", "picture", "plant", "radio", "rock", "tchotchke"];

    // Calculate level of a wavfile
    function calculateLevel(wavfile) {
      var volume;
      if (!wavfile.cal) {
        logger.warn(
          "No calibration for wavfile " + wavfile.path.slice(wavfile.path.lastIndexOf("/") + 1) + " ... playing at 25%."
        );
        noCalibrationWavfiles.push(wavfile.path.slice(wavfile.path.lastIndexOf("/") + 1));
        volume = 0.25;
      } else {
        volume = media.calculateVolume(wavfile);
      }
      return volume;
    }

    /**
     * Method to calculate Fetch levels and to save cues and masking noise wavfiles to local directory
     **/
    function calculateFetchLevels() {
      page.dm.responseArea.dataOut.data.targets = [];
      page.dm.wavfiles.forEach(function(wavfile) {
        if (wavfile.path.slice(wavfile.path.lastIndexOf("/") + 1, -4) !== "MaskingNoise") {
          vol = calculateLevel(wavfile);
          page.dm.responseArea.dataOut.data.targets.push({
            path: paths.public("") + dir + wavfile.path.slice(wavfile.path.lastIndexOf("/") + 1),
            file: wavfile.path.slice(wavfile.path.lastIndexOf("/") + 1),
            text: wavfile.path.slice(wavfile.path.lastIndexOf("/") + 1, -4),
            volume: vol
          });
        } else {
          var minVolume, maxVolume, targetSPL;
          if (!wavfile.cal) {
            logger.warn(
              "No calibration for wavfile " +
                wavfile.path.slice(wavfile.path.lastIndexOf("/") + 1) +
                " ... playing at 25%."
            );
            noCalibrationWavfiles.push(wavfile.path.slice(wavfile.path.lastIndexOf("/") + 1));
            minVolume = 0.25;
            maxVolume = 0.35;
          } else {
            targetSPL = wavfile.targetSPL || 65;
            wavfile.targetSPL = angular.isDefined(page.dm.responseArea.dataOut.data.EasiestSnrDb)
              ? targetSPL - page.dm.responseArea.dataOut.data.EasiestSnrDb
              : targetSPL - 0;
            minVolume = media.calculateVolume(wavfile);
            wavfile.targetSPL = angular.isDefined(page.dm.responseArea.dataOut.data.HardestSnrDb)
              ? targetSPL - page.dm.responseArea.dataOut.data.HardestSnrDb
              : targetSPL + 15;
            maxVolume = media.calculateVolume(wavfile);
          }
          page.dm.responseArea.dataOut.data.masker = {
            path: paths.public("") + dir + wavfile.path.slice(wavfile.path.lastIndexOf("/") + 1),
            file: "MaskingNoise.wav",
            text: "MaskingNoise",
            minVolume: minVolume,
            maxVolume: maxVolume
          };
        }
      });

      if (noCalibrationWavfiles.length > 0) {
        notifications.alert(noCalibrationWavfiles.length + " wavfiles were not found and will not be played in Fetch.");
        logger.info(noCalibrationWavfiles.length + " wavfiles were not found and will not be played in Fetch.");
      }

      if (angular.isUndefined(page.dm.responseArea.dataOut.data.masker)) {
        //        TODO: Explain how Fetch will use the min and max SNR to set masker level?
        notifications.alert(
          gettextCatalog.getString(
            'No masking noise wavfile was provided in the protocol. Please ensure that the masking noise wavfile is labeled "MaskingNoise". Otherwise, Unity will use its default, non-calibrated masking noise audio.'
          )
        );
        logger.info(
          'No masking noise wavfile was provided in the protocol. Please ensure that the masking noise wavfile is labeled "MaskingNoise". Otherwise, Unity will use its default, non-calibrated masking noise audio.'
        );
      }
    }

    /**
     * Method to calculate Computro levels and to save cues and masking noise wavfiles to local directory
     **/
    function calculateComputroLevels() {
      // Check if signal audio files are present in page.dm.wavfiles.
      // If yes choose one at random , find the corresponding transcript, and calculate the calibrated volume.
      // If not do nothing.

      // Check if masker audio files are present in page.dm.wavfiles.
      // If yes choose one at random , and calculate the calibrated volume.
      // If not do nothing.

      // Do this for each level. In heartrack protocol, there is always just 1 level since we choose which level in the preprocess function.
      page.dm.responseArea.dataOut.data.externalTargetAudioPath = [];
      page.dm.responseArea.dataOut.data.externalTargetTranscriptPath = [];
      page.dm.responseArea.dataOut.data.externalMaskers = [];

      page.dm.responseArea.dataOut.data.levels.forEach(function(lev, idx) {
        volMasker[idx] = [];
        volSignal[idx] = [];
      });

      page.dm.wavfiles.forEach(function(wav, k) {
        if (wav.path.slice(wav.path.lastIndexOf("/") + 1).length < 7) {
          page.dm.responseArea.dataOut.data.externalTargetAudioPath.push(
            paths.public("") + dir + wav.path.slice(wav.path.lastIndexOf("/") + 1)
          );
          page.dm.responseArea.dataOut.data.externalTargetTranscriptPath.push(
            paths.public("") + dir + wav.path.slice(wav.path.lastIndexOf("/") + 1, -3) + "json"
          );
          page.dm.responseArea.dataOut.data.levels.forEach(function(lev, idx) {
            wav.targetSPL = angular.isDefined(lev.signalSPL) ? lev.signalSPL : 65;
            volSignal[idx].push(calculateLevel(wav));
          });
        } else {
          page.dm.responseArea.dataOut.data.externalMaskers.push({
            audioPath: paths.public("") + dir + wav.path.slice(wav.path.lastIndexOf("/") + 1),
            visualRewardName: wav.path.slice(wav.path.lastIndexOf("/") + 1, -4)
          });
          page.dm.responseArea.dataOut.data.levels.forEach(function(lev, idx) {
            wav.targetSPL = angular.isDefined(lev.maskerSPL) ? lev.maskerSPL : 65;
            volMasker[idx].push(calculateLevel(wav));
          });
        }
      });

      page.dm.responseArea.dataOut.data.levels.forEach(function(lev, idx) {
        page.dm.responseArea.dataOut.data.levels[idx].maskerType = 1;
        page.dm.responseArea.dataOut.data.levels[idx].targetType = 1;
        page.dm.responseArea.dataOut.data.levels[idx].maskerVolume = volMasker[idx];
        page.dm.responseArea.dataOut.data.levels[idx].signalVolume = volSignal[idx];
      });

      if (noCalibrationWavfiles.length > 0) {
        notifications.alert(
          noCalibrationWavfiles.length + " wavfiles were not found and will not be played in Computro."
        );
        logger.info(noCalibrationWavfiles.length + " wavfiles were not found and will not be played in Computro.");
      }
    }

    async function sendExternalData() {
      var d;
      if (page.dm.responseArea.appName) {
        if (
          page.dm.responseArea.appName == "com.wahtshearing.audhere.testtaker" ||
          page.dm.responseArea.appName == "com.wahtshearing.audhere.allinone"
        ) {
          d = {
            message: "Tabsint Results",
            data: page.dm.response
          };

          // check if cha/wahts is connected and then disconnect if connected
          if (cha.state == "connected") {
            await cha.disconnect();
            logger.debug("WAHTS has been disconnected.");
          }

          disk.audhere = undefined; // mext time we launch tabsint, it may not be from audhere and if it is, we'll have new results
          examLogic.submit();
        } else {
          d = {
            message: page.dm.responseArea.dataOut.message || "Test InterApp Message",
            data: page.dm.responseArea.dataOut.data || "Test InterApp Data"
          };
        }
        page.result.sentToExternalApp = d;
        page.result.externalAppName = page.dm.responseArea.appName;
        tabsintReceiver.sendExternalData(
          function() {
            logger.info(
              "Successfully sent data " + JSON.stringify(d) + " to external app " + page.dm.responseArea.appName + "."
            );
          },
          function(e) {
            logger.error(
              "Sending data " +
                JSON.stringify(d) +
                " to external app " +
                page.dm.responseArea.appName +
                ".  Error: " +
                JSON.stringify(e)
            );
            var strmsg = JSON.stringify(e);
            if (strmsg.indexOf("No Activity found to handle Intent")) {
              notifications.alert(
                gettextCatalog.getString("Warning:  Error sending data to external app ") +
                  disk.interApp.appName +
                  ".  " +
                  gettextCatalog.getString(
                    "No app by that name can be found on this device.  Please alert administrator."
                  )
              );
            }
          },
          page.dm.responseArea.appName,
          d
        );
      }
    }

    function handleExternalAppData(d) {
      logger.info("Received data from app " + page.dm.responseArea.appName + ".  Data: " + JSON.stringify(d));
      if (
        page.dm.responseArea.appName == "com.wahtshearing.audhere.testtaker" ||
        page.dm.responseArea.appName == "com.wahtshearing.audhere.allinone"
      ) {
        // shouldn't get here
      } else {
        $scope.externalApp.dataIn = d;
        page.result.response = d;
        page.result.receivedFromExternalApp = d;
        if (page.dm.responseArea.autoSubmit) {
          examLogic.submit();
        }
      }
    }

    if (
      page.dm.responseArea.appName !== "com.wahtshearing.audhere.testtaker" &&
      page.dm.responseArea.appName !== "com.wahtshearing.audhere.allinone"
    ) {
      tabsintReceiver.setExternalDataHandler(handleExternalAppData, function(e) {
        logger.error("in interApp data handler: " + JSON.stringify(e));
      });
    }

    try {
      if (
        angular.isDefined(page.dm.wavfiles) &&
        (page.dm.responseArea.appName !== "com.wahtshearing.audhere.testtaker" &&
          page.dm.responseArea.appName !== "com.wahtshearing.audhere.allinone")
      ) {
        if (page.dm.responseArea.appName === "com.creare.fetch") {
          calculateFetchLevels();
        } else if (page.dm.responseArea.appName === "com.creare.computro") {
          calculateComputroLevels();
        }
      } else if (
        page.dm.responseArea.appName !== "com.wahtshearing.audhere.testtaker" &&
        page.dm.responseArea.appName !== "com.wahtshearing.audhere.allinone"
      ) {
        notifications.alert(
          gettextCatalog.getString(
            "No wavfiles are provided in the TabSINT protocol. " +
              page.dm.responseArea.appName +
              " plays default targets at uncalibrated levels. Press OK to continue."
          )
        );
        logger.info(
          "No wavfiles are provided in the TabSINT protocol. " +
            page.dm.responseArea.appName +
            " plays default targets at uncalibrated levels."
        );
      }
    } catch (err) {
      notifications.alert(
        gettextCatalog.getString(
          "An error occurred while processing the wavfile volumes for " +
            page.dm.responseArea.appName +
            ". Error: " +
            err.toString() +
            ". Please check your calibrated protocol and try again."
        )
      );
      logger.error(
        "An error occurred while processing the wavfile volumes for " +
          page.dm.responseArea.appName +
          ". Error: " +
          err.toString()
      );
    }
    sendExternalData();
  });
