/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

angular
  .module("tabsint.components.response-areas.image-map", [])

  .directive("coordTranslate", function() {
    return {
      restrict: "A",
      link: function(scope, element, attrs) {
        element.on("load", function() {
          // get image width and height, determined by the percentage in protocol
          let w = element[0].width,
            h = element[0].height;

          angular.forEach(angular.element(document.querySelector("#hotspotMap")).find("area"), function(val, key) {
            let $this = angular.element(val);

            if (!$this.data("coords")) {
              $this.data("coords", $this.attr("coords"));
            }

            let coords = $this.data("coords").split(","),
              coordsPercent = new Array(coords.length),
              shape = $this.attr("shape");

            // Determine the size in image pixels
            for (var i = 0; i < coordsPercent.length; ++i) {
              if (i % 2 === 0) {
                coordsPercent[i] = parseInt(Math.round((coords[i] / 100) * w));
              } else {
                coordsPercent[i] = parseInt(Math.round((coords[i] / 100) * h));
              }
            }
            $this.attr("coords", coordsPercent.toString());
          });
        });
      }
    };
  })

  .controller("ImageMapResponseAreaCtrl", function($scope, examLogic, page, $location, $anchorScroll) {
    $scope.image = page.dm.responseArea.image;

    function update() {
      $scope.hotspots = page.dm.responseArea.hotspots;
      for (var i = 0; i < $scope.hotspots.length; i++) {
        if ($scope.hotspots[i].other) {
          $scope.hotspots[i].id = "Other"; // set ID to other to trigger 'Other' response
        }
      }
    }

    $scope.$watch("page.dm.responseArea.hotspots", update());
    update();

    // Function when a hotspot is chosen
    $scope.choose = function(id) {
      // AUTO-SUBMIT:
      page.result.response = id;
      if (page.result.response !== "Other") {
        examLogic.submit();
      }
    };

    $scope.$watch("page.result.response", function() {
      if (page.result.response === "Other") {
        $scope.enableOtherText = true;
        // $location.hash('otherInput');
        $anchorScroll();
      } else {
        $scope.enableOtherText = false;
        page.result.otherResponse = undefined;
      }
    });
  });
