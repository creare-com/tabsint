/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.dpoae-table", [])
  .directive("dpoaeTable", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/dpoae/dpoae-table.html",
      controller: "dpoaeTableCtrl"
    };
  })

  .controller("dpoaeTableCtrl", function($scope, page, chaResults) {
    // Logic
    page.dm.hideProgressbar = true;
    page.dm.isSubmittable = true;

    // Update page view, get rid of instructions
    $scope.page.dm.title = "DPOAE Results";
    $scope.page.dm.questionMainText = "";
    $scope.page.dm.instructionText = "";

    if (angular.isUndefined($scope.resultsList)) {
      console.log(page.dm.responseArea.displayIds, page.dm.responseArea.displayIds);
      var results = chaResults.getPastResults(page.dm.responseArea.displayIds, null);
      $scope.tableResults = chaResults.createDPOAETableData(results);
    }
  });
