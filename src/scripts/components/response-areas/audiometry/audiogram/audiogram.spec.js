/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../../../app";

beforeEach(angular.mock.module("tabsint"));
//TODO: Add more tests for audiogram embedded in audiometry results, automated audiometry results,
// masking symbol replacement, etc.
describe("repeatIfFailedOnce: Bekesy Audiometry ", function() {
  var $compile, $rootScope, scope, page;

  beforeEach(function() {
    angular.mock.inject(function(_$rootScope_, _$compile_, _$httpBackend_, _page_) {
      _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");
      $rootScope = _$rootScope_;
      // $window = _$window_;
      $compile = _$compile_;
      scope = $rootScope.$new();
      page = _page_;
      page.dm = {};
    });
    // $window.resolveLocalFileSystemURL = function() {
    //   return {};
    // };
    // spyOn($window, "resolveLocalFileSystemURL").and.returnValue($window.resolveLocalFileSystemURL);
  });

  it("Replaces div with a d3 plot", function() {
    let audiogramData = {
      frequency: [1000],
      level: [30],
      channel: ["left"],
      ResultType: ["Threshold"],
      masking: [false],
      xLabel: "Frequency (Hz)",
      yLabel: "Hearing Level (dB HL)"
    };
    scope.audiogramData = audiogramData;
    scope.$digest();
    let element = $compile('<audiogram-plot audiogram-data="audiogramData"></audiogram-plot>')(scope);

    // did d3Service add an svg to the div?
    expect(element.html()).toContain("svg");
    // did specified x/y labels get set?
    expect(element.html()).toContain("Hearing Level (dB HL)");
    expect(element.html()).toContain("Frequency (Hz)");
  });
});
