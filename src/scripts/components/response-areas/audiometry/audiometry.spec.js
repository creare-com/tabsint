/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../../app";

beforeEach(angular.mock.module("tabsint"));

xdescribe("repeatIfFailedOnce: Bekesy Audiometry ", function() {
  var scope, page, cha, chaExams, $rootScope, $controller;

  beforeEach(
    angular.mock.inject(function(_$controller_, _$rootScope_, _page_, _cha_, _chaExams_, _$httpBackend_) {
      _page_.result = {
        response: undefined
      };

      page = _page_;

      cha = _cha_;
      _chaExams_.wait = {
        forReadyState: async function() {
          chaExams.state = "exam";
          return true;
        }
      };
      _chaExams_.getChaInfo = function() {
        return "";
      };
      chaExams = _chaExams_;
      _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");
      $rootScope = _$rootScope_;
      $controller = _$controller_;
      scope = $rootScope.$new(); // start a new scope
    })
  );

  it("should have default options defined", function() {
    cha.requestResults = async function() {
      return { ResultType: "Crap" };
    };
    page.dm = {
      responseArea: {
        type: "BekesyMLD",
        repeatIfFailedOnce: true,
        examProperties: {
          UseSoftwareButton: true,
          PresentationMax: 30,
          F: 2000,
          Lstart: 30,
          MaskerEar: 2,
          MaskerPhase: 0,
          TargetEar: 2,
          TargetPhase: 180
        }
      }
    };
    chaExams.setup(page.dm.responseArea.type, page.dm.responseArea.examProperties);
    scope.page = page;

    $controller("AudiometryExamCtrl", {
      $scope: scope
    });
    scope.$digest();

    // spyOn(chaExams, "wait.forReadyState").and.callThrough();
    // expect(chaExams.wait.forReadyState).toHaveBeenCalled();
    // debugger;
    expect(page.result.failedOnce).toBeUndefined();
  });
});
