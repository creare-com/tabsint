/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.audiometry-service", [])
  .factory("chaAudiometryService", function($timeout, $q, page, cha, chaExams, logger) {
    var api = {
      reset: undefined,
      logButtonPress: undefined,
      buttonPressCount: 0,
      buttonPressTimes: [],
      simulateTimer: undefined,
      examDone: false
    };

    api.reset = function() {
      api.buttonPressCount = 0;
      api.examDone = false;
      api.startTime = new Date();
      api.buttonPressTimes = [];
      if (api.simulateTimer) {
        $timeout.cancel(api.simulateTimer);
      }
    };

    api.logButtonPress = function() {
      api.buttonPressCount++;
      api.buttonPressTimes.push(new Date() - api.startTime);
    };

    // Results processing - results will be available aftwarwards on page.result
    api.processResults = function(results) {
      if (api.simulateTimer) {
        $timeout.cancel(api.simulateTimer);
      }

      // make page unsubmittable while processing
      page.dm.isSubmittable = false;

      // convert integers to string results, save exam results to structure
      results.chaInfo = chaExams.getChaInfo();
      results.examProperties = chaExams.examProperties;
      results.examType = chaExams.examType;
      results.buttonPressTimes = api.buttonPressTimes;

      // change ResultType if its a screener exam
      if (results.examProperties.Screener) {
        if (results.ResultType === "Threshold") {
          results.ResultType = "Pass";
        } else if (results.ResultType === "Hearing Potentially Outside Measurable Range") {
          results.ResultType = "Unused";
        } else if (results.ResultType === "Failed to Converge") {
          results.ResultType = "Fail";
        }
      }

      page.result = $.extend({}, page.result, results);

      // set 'response' field to Threshold, or if no threshold, display the result Type
      if (results.Threshold !== undefined) {
        page.result.response = results.Threshold;
      } else {
        page.result.response = results.ResultType;
      }
    };

    return api;
  });
