/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.third-octave-bands", [])
  .directive("thirdOctaveBandsExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/third-octave-bands/third-octave-bands.html",
      controller: "ThirdOctaveBandExamCtrl"
    };
  })
  .controller("ThirdOctaveBandExamCtrl", function(
    $scope,
    $timeout,
    cha,
    chaExams,
    chaResults,
    examLogic,
    page,
    logger
  ) {
    if (page.dm.responseArea.displayMode == "Vanilla") {
      $scope.TOBType = "Vanilla";
    } else {
      $scope.TOBType = "MPANL";
    }

    var resultList = [];
    var startTime = new Date();
    examLogic.submit = function() {
      submitResults();
    };

    $scope.page.dm.questionMainText = "Background Noise Measurement";
    $scope.page.dm.questionSubText =
      page.dm.examInstructions || "Please sit quietly while we measure the background noise levels.";

    function presentResults() {
      chaExams.state = "results";
      page.dm.hideProgressbar = true;

      $scope.page.dm.title = "Results";
      $scope.page.dm.questionMainText = "Background Noise Results";
      $scope.page.dm.questionSubText = "";
      $scope.page.dm.instructionText = "";

      page.result.response = "continue";

      if (page.dm.responseArea.autoSubmit) {
        examLogic.submit();
        return;
      } else {
        page.dm.isSubmittable = true;
      }

      var figures = chaResults.createTOBFigures(resultList, page.dm.responseArea.standard);

      $scope.thirdOctaveBandData = figures[1];
      $scope.thirdOctaveBandData.TOBType = $scope.TOBType;

      if ($scope.TOBType == "Vanilla") {
        $scope.tableResults = $scope.thirdOctaveBandData[Object.keys($scope.thirdOctaveBandData)[0]];

        let tob_freqs = [];
        let tob_levels = [];
        for (const [key, value] of Object.entries($scope.tableResults)) {
          tob_freqs.push(value.freq);
          tob_levels.push(value.level);
        }
        $scope.LAeq = chaResults.calculateLAeq(tob_freqs, tob_levels);
      }
    }

    function submitResults() {
      function submitSingleResult(pres) {
        page.result = pres;
        logger.debug("CHA pushing maunal audiometry presentation result onto stack: " + angular.toJson(page.result));
        examLogic.pushResults();
      }

      if (resultList.length > 1) {
        _.forEach(resultList, submitSingleResult);

        page.result = {
          presentationId: "ThirdOctaveBands",
          responseStartTime: startTime,
          response: "complete"
        };
      } else {
        page.result = $.extend({}, page.result, resultList[0]);
      }

      examLogic.submit = examLogic.submitDefault;
      examLogic.submit();
    }

    function addResults(results) {
      if (results.Frequencies === angular.undefined || results.Leq === angular.undefined) {
        results.dataError = "No results returned";
        $scope.dataError = results.dataError;
      }

      // Add the ear to the results
      if (examLogic.dm.state.flags.startRight) {
        results.startRight = true;
      } else {
        results.startRight = undefined;
      }

      // Note results object can contain db SPL values of 1000 which is a placeholder for NaN
      results.examProperties = chaExams.examProperties;
      results.examType = chaExams.examType;
      results.responseStartTime = startTime;
      results.chaInfo = chaExams.getChaInfo();
      var tmpResult = $.extend({}, page.result, results);

      resultList.push(angular.copy(tmpResult));
    }

    $scope.repeatTOB = function() {
      logger.debug("repeating TOB");

      if (page.dm.responseArea.measureBothEars) {
        chaExams.examProperties.InputChannel = "SMICR0";
      }

      cha
        .requestStatusBeforeExam()
        .then(function() {
          return cha.queueExam(chaExams.examType, chaExams.examProperties);
        })
        .then(handleResults)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    };

    function runRightEar() {
      chaExams.examProperties.InputChannel = "SMICR1";
      return cha
        .requestStatusBeforeExam()
        .then(function() {
          return cha.queueExam(chaExams.examType, chaExams.examProperties);
        })
        .then(chaExams.wait.forReadyState)
        .then(function() {
          return cha.requestResults();
        })
        .then(function(results) {
          addResults(results);
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    function handleResults() {
      chaExams.wait
        .forReadyState()
        .then(function() {
          return cha.requestResults();
        })
        .then(function(results) {
          addResults(results);
        })
        .then(function() {
          if (page.dm.responseArea.measureBothEars) {
            return runRightEar();
          }
        })
        .then(presentResults)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    if (angular.isDefined(page.dm.responseArea.delay)) {
      $timeout(handleResults, page.dm.responseArea.delay);
    } else {
      handleResults();
    }
  })

  .directive("thirdOctaveBandPlot", function() {
    return {
      restrict: "E",
      template: '<div id="thirdOctaveBandPlotWindow"></div>',
      controller: "ThirdOctaveBandPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("ThirdOctaveBandPlotCtrl", function($scope, d3Services) {
    d3Services.thirdOctaveBandPlot("#thirdOctaveBandPlotWindow", $scope.data);
  });
