/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.auto-shutdownTimer", [])

  .factory("changeTimer", function($uibModal, cha, logger, disk, notifications) {
    var api = {
      editShutdownTimer: undefined
    };

    /**
     * Check that user is authorized using a modal dialog, and if so, call targetFn.
     * @param  {function} targetFn - callback function upon success
     * @param  {boolean} skipAuthorize - override the authorization (i.e. admin mode)
     */
    api.editShutdownTimer = function() {
      var ModalInstanceCtrl = function($scope, $uibModalInstance) {
        $scope.save = function(shutdownTimer) {
          $uibModalInstance.close(shutdownTimer);
        };
        $scope.cancel = function() {
          $uibModalInstance.dismiss("cancel");
        };
      };

      var modalInstance = $uibModal.open({
        templateUrl: "scripts/components/auto-shutdownTimer/auto-shutdownTimer.html",
        controller: ModalInstanceCtrl,
        backdrop: "static"
      });

      modalInstance.result.then(function(shutdownTimer) {
        if ((shutdownTimer >= 5) & (shutdownTimer <= 600) & (shutdownTimer === parseInt(shutdownTimer, 10))) {
          disk.shutdownTimer = shutdownTimer;
          logger.info("User updated shutdown timer to: " + shutdownTimer);
          try {
            cha.myCha.writeSetting("auto_shutdown_time", shutdownTimer);
            cha.myCha.requestSetting("auto_shutdown_time");
          } catch (e) {
            logger.info("WAHTS Shutdown Timer not Supported");
            notifications.alert("WAHTS Shutdown Timer not Supported");
          }
        } else {
          logger.warn("User tried to update shutdown timer, entered: " + shutdownTimer);
        }
      });
    };

    return api;
  });
