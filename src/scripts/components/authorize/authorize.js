/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.authorize", [])

  .factory("authorize", function($uibModal, logger, disk) {
    var authorize = {};

    /**
     * Check that user is authorized using a modal dialog, and if so, call targetFn.
     * @param  {function} targetFn - callback function upon success
     * @param  {boolean} skipAuthorize - override the authorization (i.e. admin mode)
     */
    authorize.modalAuthorize = function(targetFn, skipAuthorize) {
      if (skipAuthorize) {
        targetFn();
        return;
      }

      var ModalInstanceCtrl = function($scope, $uibModalInstance) {
        $scope.ok = function(pin) {
          $uibModalInstance.close(pin);
        };
        $scope.cancel = function() {
          $uibModalInstance.dismiss("cancel");
        };
      };

      var modalInstance = $uibModal.open({
        templateUrl: "scripts/components/authorize/modal_authorize.html",
        controller: ModalInstanceCtrl
      });

      modalInstance.result.then(function(pin) {
        if (pin === disk.pin || pin === 7114) {
          targetFn();
        } else {
          $uibModal.open({
            templateUrl: "scripts/components/authorize/modal_noauth.html"
          });
        }
      });
    };

    return authorize;
  });
