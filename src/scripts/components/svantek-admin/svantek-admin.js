/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.svantek.admin", [])

  .directive("svantekAdmin", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/svantek-admin/svantek-admin.html",
      controller: "SvantekAdminCtrl",
      scope: {}
    };
  })
  .controller("SvantekAdminCtrl", function($scope, svantek, bluetoothStatus, disk) {
    $scope.svantek = svantek;
    $scope.disk = disk;
    $scope.bluetoothStatus = bluetoothStatus;
  });
