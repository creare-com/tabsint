# TabSINT Receiver

This plugin is built to provide utilities for inter-app communication with TabSINT (https://gitlab.com/creare-com/tabsint) on Android devices.

## Use


### Send Data

`sendExternalData(successCallback, errorCallback, externalAppName, data)`

externalAppName: string with format com.organizationName.projectGroup.project
data: JSON.stringify({message: 'your message', data: anyDataToSend})

Send an intent to a second app.  Data is optional.

Behavior Cases:

* Second app is not currently open: the intent will open the app, and then switch focus to the second app.
* Second app is already open: the intent will simply switch focus to the app.
* Second app does not exist: error notifications

### Receiver Initialization

To listen for message intents from other apps, define dataHandler(d) function, where d is a string of JSON data.

Set the dataHandler using `TabSINTReceiver.setExternalDataIntentHandler(dataHandler, errCallback)`.  This handler will
be called each time in the *future* that the app receives an intent.  To recover the data associated with the initial
intent, before the handler was set, call `TabSINTReceiver.getExternalDataIntent(dataHandler, errCallback)`.


```
function dataHandler(d) {

  // Ensure data is defined and correct type
  if (d.type && d.type === 'text/plain' && d.data) {
    try {
      var dataIn = JSON.parse(d.data);
      console.log('Received interapp message.');
      console.log(dataIn);

      //.... do something with the data
      
    } catch (e) {
      console.log('ERROR: Could not parse incoming data object.  Error: ' + JSON.stringify(e) + '.  The incoming data object should be a JSON.stringified object.  Received: ' + d);
    }
  }
  
}

// It is possible to open another app with the sendMessage command if that app is not already open.  
// By the time the app is loaded and a dataHandler is assigned, any included message has already come in, 
// and the intent dataHandler will not get fired for data already received.  Use getExternalDataIntent
// to capture the message and data with the initial intent.
TabSINTReceiver.getExternalDataIntent(dataHandler, errCallback);
TabSINTReceiver.setExternalDataIntentHandler(dataHandler, errorCallback);

```
