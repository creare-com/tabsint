"""
This script reads in TabSINT JSON results, flattens them, and writes them to CSV
"""

import json
import os
from pandas.io.json import json_normalize


def json2csv(file):
    with open(file, 'r') as result_file:
        result = json.load(result_file)   # load json file into result dictionary

    # flatten json results into pandas dataframe
    df = json_normalize(result['testResults']['responses'])  

    # update column names
    leading_cols = ['responseStartTime', 'presentationId', 'responseArea', 'response', 'examType']  # shown in order first
    cols = []
    for name in df.columns:
        if name.startswith('chaInfo.'):
            cols.append(name[8:])
        elif name.startswith('examProperties.'):
            cols.append(name[15:])
        elif name not in leading_cols:
            cols.append(name)


    # copy top level exam properties into csv. Don't fail if the property is not in the result
    exam_cols = ['testDateTime', 'protocolName', 'tabletUUID', 'subjectId']
    for col in exam_cols:
        try: 
            df[col] = result[col]
            leading_cols.insert(0, col)
        except Exception:
            pass
    
    # order the columns: leading_cols, then cols
    df = df.reindex_axis(leading_cols + cols , axis=1)
    
    # write out to csv file
    df.to_csv(file[:-4]+'csv')

if __name__ == '__main__':
    files = os.listdir('.')         # list all files in the directory
    for item in files:
        if item.endswith('.json'):  # process only files ending in .json
            json2csv(item)  
