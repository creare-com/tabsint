function [b,a,a_w_discrete]=define_a_weighting_timeDomain(fs);
%function [b,a]=define_a_weighting_ztransform(fs);
%Created: Chip Audette, July 29, 2002
%Purpose: Define the time-domain filter constants for a "A"
%         weighting of audio.  Output of this filter
%         is for use with the filter command.
%References: from http://www.ptpart.co.uk/noise.htm I obtained
%         the following transfer function that supposedly defines
%         the A-Weighting filter...
%
%                              ka*s^4
%         ------------------------------------------------
%         (s+129.4)^2 * (s+676.7) * (s+4636) * (s+76655)^2
%


% %Define transfer function  
% num=poly([0 0 0 0]);
% den1=poly(-129.4);
% den2=poly(-676.7);
% den3=poly(-4636);
% den4=poly(-76655);
% den=conv(conv(conv(conv(conv(den1,den1),den2),den3),den4),den4);
% a_weight=tf(num,den);
% 
% %Scale transfer function to yield 0dB response at 1000 Hz
% offset = 10*log10(abs(freqresp(a_weight,1000*2*pi)));
% num=(10^(0.1*(-offset)))*poly([0 0 0 0]);
% a_weight=tf(num,den);
% %Convert from transfer function to discrete time system
% a_w_discrete = c2d(a_weight,1/fs,'matched');
% 
% %get filter parameters from the coeffients of the numerator
% %and denominator of the discrete transfer function
% b=a_w_discrete.num{1};
% a=a_w_discrete.den{1};

% Above is code to calculate filter coefficients using control toolbox
b = [0,0.377893250842294,-1.133679752526882,0.755786501684588,0.755786501684588,-1.133679752526882,0.377893250842294];
a = [1,-4.293565492892926,7.283448991867394,-6.124274146445076,2.608983232916493,-0.511107612145960,0.036515032641141];


