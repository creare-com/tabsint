function [cumulative_mat] = calibrate_files(protocol_json, ...
    write_folder,           ...
    cumulative_mat,         ...
    hardware_spec_file,     ...
    cal_filter,             ...
    ref_name,               ...
    ref_level,              ...
    headset                 ...
    )
    % Calibrate sound files for the Skilled Hearing Task I tabSINT application.
    % A direct implementation of JCW's "Server-Side Calibration" flowchart.
    % This function will write calibrated .wav, and append meta-data (relevant
    % flags and levels) to dictionary cumulative_mat.
    %
    % Arguments:
    % protocol_json: (struct) calibration data opened from
    %   protocol.json file in calibrate_protocol.m
    % write_folder: (string) Name of folder that calibrated files will 
    %   be saved to         
    % cumulative_mat: (struct) Structure for saving meta data
    % hardware_spec_file: (string) name of .mat file in 
    %   hardware_spec_files coordinating to the headset and tablet being
    %   being used. Contains fields 'scaleFactor' and 'b_fir'      
    % cal_filter: (string) type of calibration filter either "full" or
    %   or "flat"
    % ref_name: (optional,string) filename of a reference .wav file (no path)        
    % ref_level: (optional) LEQ (dB SPL) associated with the reference file
    % headset: (string) Type of headset being calibrated for
    %
    % Outputs:
    % cumulative_mat: (struct) A matlabl structure containing calibrated
    % audio data and all of the meta data from the calibration 
    %
    % From skilled_hearing_calibration.py - MCR 2014
    % Conversion to MATLAB - MSwanson 1/13/2023

    % Load hardware_spec .mat file
    try
        hardware_spec = load(append('Hardware_spec_files/',hardware_spec_file));
    catch
        error('Error: Cannot load hardware spec file. Check that the "Hardware_spec_files" folder is in the working directory.')
    end

    % Check if a reference .wav file is defined
    if ~strcmp(ref_name,'')
        % Initialize calibrated_wav_file class for the reference file
        ref_file = calibrated_wav_file(ref_name,write_folder,1);
    end
    
    %Initialize struct to save calibrated audio data for cumulative_mat
    wav_data = struct();

    % Initialize calibrated_wav_file, run Calibrate function, and write
    % each calibrated .wav file
    for i = 1:length(protocol_json.calibration.wavfiles)
        % Define file_name that will be calibrated
        file_name = protocol_json.calibration.wavfiles(i);
        % Check if the headset is set to Audiometer, 

        if strcmp(headset, 'Audiometer')
            % Initialize calibrated_audiometer_file class,
            wav = calibrated_audiometer_file(file_name,write_folder);
            % Add meta data to class
            wav.meta = struct();
            wav.meta.wavRMSZ = 1;
            wav.meta.scaleFactor = 1;
            wav.meta.realWorldRMSZ = 1;
            wav.meta.refType = 'as-recorded';
        else
            % Initialize calibrated_wav_file class for file
            wav = calibrated_wav_file(file_name,write_folder,i);
        end

        % Set meta data for 2 possible calibration types, first checks for
        % audiometer.
        % 1. Reference file and level exists -> as-recorded calibration
        % 2. No Reference file -> arbitrary calibration
        if strcmp(headset, 'Audiometer')
            wav.meta(i).refType = 'as-recorded';
        elseif ~isempty(ref_name)  % Reference file exists
                % Create calibration factor based on reference file RMS
                pa_rms = 20e-6 * 10^(ref_level/20);
                cal_factor = pa_rms./ref_file.meta.RMSZ;
                
                % if channel is silent change calibration factor to 0
                if cal_factor(1) || cal_factor(2) == inf
                    inf_index = cal_factor == Inf;
                    cal_factor(inf_index) = 0;
                end
                % Set meta(i) data
                wav.meta(i).refType = 'as-recorded';
                wav.meta(i).realWorldRMSA = wav.meta(i).RMSA.*cal_factor;
                wav.meta(i).realWorldRMSC = wav.meta(i).RMSC.*cal_factor;
                wav.meta(i).realWorldRMSZ = wav.meta(i).RMSZ.*cal_factor;
        else  % Run arbitrary calibration
            % Set meta data
            wav.meta(i).refType = 'Arbitrary';
        end
    
        % Calibrate file if headset is not an Audiometer
        if ~strcmp(headset, 'Audiometer')
            % Run calibrated_wav_file class function calibrate
            wav = wav.calibrate(file_name, hardware_spec,cal_filter,i);
        end
        
        % Add meta data in to cumulative_mat
        cumulative_mat.meta(i) = wav.meta(i);
        wav_data(i).data = wav.cal_wav;
       
        % Write wav file to calibrated folder
        try
            write_path = append(write_folder,'/',file_name);
            audiowrite(write_path,wav.cal_wav,wav.Fs);
        catch
            error(append('Error: Failed to write calibrated .wav file for "',file_name,'". Check write path and filename'))
        end
    end
    
    % Rearrange meta data because TabSint doesn't like MATLAB class
    % structures
    calibration_json = struct();
    calibration_json.headset = cumulative_mat.headset;
    calibration_json.tablet = cumulative_mat.tablet;
    calibration_json.calibrationPySVNRevision = 'unavailable';
    calibration_json.calibrationPyManualReleaseDate = 20230321;
    calibration_json.audioProfileVersion = hardware_spec.versionDate;
    
    % Order meta data properly 
    for i = 1:length(cumulative_mat.wavfiles)
        % Add special character to file name to replace later
        file = strrep(cumulative_mat.wavfiles(i), '.wav','');

        % Set Meta Data for as-recorded calibrations
        if strcmp(cumulative_mat.meta(i).refType,'as-recorded')
            calibration_json.(file).refType = cumulative_mat.meta(i).refType;
            calibration_json.(file).RMSC = cumulative_mat.meta(i).RMSC;
            calibration_json.(file).realWorldRMSA = cumulative_mat.meta(i).realWorldRMSA;
            calibration_json.(file).RMSA = cumulative_mat.meta(i).RMSA;
            calibration_json.(file).wavRMSC = cumulative_mat.meta(i).wavRMSC;
            calibration_json.(file).wavRMSA = cumulative_mat.meta(i).wavRMSA;
            calibration_json.(file).calibrationFilter = cumulative_mat.meta(i).calibrationFilter;
            calibration_json.(file).wavRMSZ = cumulative_mat.meta(i).wavRMSZ;
            calibration_json.(file).normFactor = cumulative_mat.meta(i).normFactor;
            calibration_json.(file).RMSZ = cumulative_mat.meta(i).RMSZ;
            calibration_json.(file).realWorldRMSZ = cumulative_mat.meta(i).realWorldRMSZ;
            calibration_json.(file).realWorldRMSC = cumulative_mat.meta(i).realWorldRMSC;
            calibration_json.(file).scaleFactor = cumulative_mat.meta(i).scaleFactor;
        
        else 
            calibration_json.(file).refType = cumulative_mat.meta(i).refType;
            calibration_json.(file).RMSC = cumulative_mat.meta(i).RMSC;
            calibration_json.(file).calibrationFilter = cumulative_mat.meta(i).calibrationFilter;
            calibration_json.(file).wavRMSC = cumulative_mat.meta(i).wavRMSC;
            calibration_json.(file).wavRMSA = cumulative_mat.meta(i).wavRMSA;
            calibration_json.(file).RMSA = cumulative_mat.meta(i).RMSA;
            calibration_json.(file).wavRMSZ = cumulative_mat.meta(i).wavRMSZ;
            calibration_json.(file).RMSZ = cumulative_mat.meta(i).RMSZ;
            calibration_json.(file).normFactor = cumulative_mat.meta(i).normFactor;
            calibration_json.(file).scaleFactor = cumulative_mat.meta(i).scaleFactor;
        end
    end
    
    % Add calibrated wav data to cumulative_mat
    cumulative_mat.wav = wav_data;

    % Encode meta data into json format
    encoded = jsonencode(calibration_json);
    for i = 1:length(cumulative_mat.wavfiles)
        encoded = strrep(encoded,strrep(cumulative_mat.wavfiles(i), '.wav',''),cumulative_mat.wavfiles(i));
    end

    % Save calibration.json file to calibrated folder
    fid = fopen(append(write_folder,'\calibration.json'),'w');
    fprintf(fid,'%s',encoded);
    fclose(fid);
    
    % Save meta data as a .mat to calibrated folder
%     save(append(write_folder,'/','Calibration_data.mat'),"cumulative_mat")

end 
