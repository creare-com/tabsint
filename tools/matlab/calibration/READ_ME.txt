MSwanson
Creare, Inc.
Created: 1-5-2023
Headset and Tablet .wav Calibration

To Run:
  1. Make a zip folder containing a 'protocol.json' and the .wav files 
     that need calibration. Optional: include a reference .wav file 
  2. Open Run_Calibration.m from the Calibrate_wav folder
  3. Select 'Calibrate_wav' folder as MATLAB directory using 'Browse for
     folder' button
  4. Hit 'Run'
  5. Select the desired zip folder to be calibrated
  6. A new folder titled 'Calibrated' containing the calibrated .wav files,
     a 'calibration.json' file, and the 'protocol.json' file will appear in
     in the working directory
  7. Upload the calibrated files to a TabSINT device

Alternatively:
  - The 'calibrate_protocol()' function can be called in a MATLAB script to 
    calibrate a zip folder
  - 'calibrate_protocol(source_zip_path)' requires a source_zip_path argument
    which is a string path to the zip folder to be calibrated
	
**Note: Avoid using special characters in file names
        All file names are caps sensitive and must match the file names in the 
            'protocol.json' calibration block
        Zipping a folder will return a folder to the "Calibrated" folder
        Zipping files will return files to the "Calibrated" folder**

The following files/folder should be located in the working directory 
or added to the path to run calibration:
  'calibrated_wav_file.wav'
  'calibrated_audiometer_file.m', 
  'calibrate_protocol.m', 
  'calibrate_files.m',
  'Hardware_spec_files' folder 

    A valid 'protocol.json' file is required to run the calibration. The 
'protocol.json' file must contain a calibration block. Put the calibration 
block in the same 'protocol.json' file that you will be uploading to TabSINT.
An example of a valid calibration block within the 'protocol.json' is 
demonstrated at the bottom of this document for an "as-recorded" and an 
"arbitrary" calibration.

2 methods of calibration:
  1. Arbitrary: In this case, a sound file is played with a volume such 
     that the A, C, or Z weighted LEQ of the output is equal to a 
     specified target.
     To run: do not define a "referenceFile" or "referenceLevel" in the protocol.json
  2. As-Recorded: In this case, a reference sound file with a known, 
     fixed LEQ should be recorded during the same session as the sound files
     to be calibrated. The playback volume is calculated such that the
     output LEQ of the reference file would be equal to the known value, 
     then the target sound file is played at that volume. This allows the 
     sound files to be played back at the same level as they were recorded.
     To run: define a "referenceFile" and "referenceLevel" in the protocol.json

Default Headset - VicFirth
Default Tablet  - TabE
Default Filter  - Full

**'protocol.json' as-recorded calibration example**
{
  "title": "MRTMRT",
  "Tablet": "TabE",
  "headset": "VicFirth",
  "calibration": [ 
    { 
      "wavfiles": [ 
        "S1_Talker3_PALE_99_Noise_70_Mono.wav"
        ], 
      "referenceFile": "mrtstd74.wav", 
      "referenceLevel": 74
    }
  ]
  {
    "pages": [
      {
        "wavfiles":[
          { 
            "path": "S1_Talker3_PALE_99_Noise_70_Mono.wav", 
            "playbackMethod": "as-recorded"
          } 
        ]
      }
    ]
  }
}



**'protocol.json' arbitrary calibration example**
{
  "title": "MRTMRT",
  "Tablet": "TabE",
  "headset": "VicFirth",
  "calibration": [ 
    { 
      "wavfiles": [ 
        "S1_Talker3_PALE_99_Noise_70_Mono.wav"
        ]
    }
  ]
  {
    "pages": [
      {
        "wavfiles":[
          { 
            "path": "S1_Talker3_PALE_99_Noise_70_Mono.wav", 
            "playbackMethod": "arbitrary",
            "TargetSPL": 74
          } 
        ]
      }
    ]
  }
}
