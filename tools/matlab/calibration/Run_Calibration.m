% MSwanson
% Creare, Inc.
% Created: 1-5-2023
% Headset and Tablet .wav Calibration

% To Run:
%   1. Make a zip folder containing a 'protocol.json' and the .wav files 
%      that need calibration. Optional: include a reference .wav file 
%   2. Open Run_Calibration.m from the Calibrate_wav folder
%   3. Select 'Calibrate_wav' folder as MATLAB directory using 'Browse for
%      folder' button
%   4. Hit 'Run'
%   5. Select the zip folder to be calibrated
%   6. A new folder titled 'Calibrated' containing calibrated .wav files,
%      calibration.json, protocol.json, and reference .wav files (optional)


% Runs Calibration
prompt = 'Please select zip folder for calibration.';
[file,path] = uigetfile('*.zip',prompt);
source_zip_file = append(path,file);
calibrate_protocol(source_zip_file);
disp('Calibration Complete')


% This is an alternative to using the HFFD server for calibratin sound
% files. Based on the skilled_hearing_calibration.py. This is an
% implementation of JCW's flowcharts IM-21-10-181

% The following files/folder should be located in the 'Calibrate_wav' 
% folder which are required to run calibration: 
%   'calibrated_wav_file.m', 
%   'calibrated_audiometer_file.m', 
%   'calibrate_protocol.m', 
%   'calibrate_files.m',
%   'Hardware_spec_files' folder 

% 2 methods of calibration:
%   1. Arbitrary: In this case a sound file is played with a volume such 
%      that the A, C, or Z weighted LEQ of the output is equal to a 
%      specified target.
%   2. As-Recorded: In this case, a reference sound file with a known, 
%      fixed LEQ (specified at upload time) must accompany the target sound 
%      file. The playback volume is calculated such that the output LEQ of 
%      the reference file would be equal to the known value, then the 
%      target sound file is played at that volume. This allows playback at 
%      real world recorded levels.

% Default Headset - VicFirth
% Default Tablet - TabE