function [cumulative_mat] = calibrate_protocol(source_zip_file, ...
    write_folder ,          ...
    tablet ,                ...
    headset                 ...
    )
    % Load in zip file that has a protocol.json file, .wav files to be
    %   calibrated, and optional .wav reference file
    % Calibrate wav files in source_path as specified in the protocol.json 
    %   file residing in that location
    % Returns the tablet and headset used for the calibration in a dict 
    %   {'tablet','headset'}
    % Default headset is VicFirth and Default Tablet is TabE
    %
    % Arguments:
    % source_zip_file: (string) The name or path of the zip file that 
    %   contains a protocol.json file and .wav files to be calibrated. 
    %   Name must contain .zip extension
    % write_folder: (string) Name of folder that calibrated files will 
    %   be saved to
    % tablet: (string) Type of Tablet being calibrated for 
    % headset: (string) Type of headset being calibrated for
    %
    % Outputs:
    % cumulative_mat: (struct) A matlabl structure containing calibrated
    % audio data and all of the meta data from the calibration 
    %
    % From skilled_hearing_calibration.py - MCR 2014
    % Conversion to MATLAB - MSwanson 1/13/2023

    % Set default parameters if arguments are not defined
    if nargin < 2
        write_folder = 'Calibrated';
    end
    if nargin < 3
        tablet = 'TabE';
    end
    if nargin < 4
        headset = 'VicFirth';
    end
    
    % Create write folder to save files to
    if ~exist(write_folder, 'dir')
        mkdir(write_folder);
    end
    
    % Open zip file
    try 
        % Opens and saves zip folder
        fileNames = unzip(source_zip_file,write_folder);
    catch
        % Give error message if no zip file
        error(append('Error opening zip folder or copying it into ',write_folder))
    end
    
    % Check if the zipped folder contains a folder
    if any(isfolder(strrep(fileNames,'protocol.json','')))
        % Get the index of the folder name 
        folder_logic = isfolder(strrep(fileNames,'protocol.json',''));
        folderNames = strrep(fileNames,'protocol.json','');
        % Replace write folder with 'Calibrated\' + Name of zipped folder
        write_folder = string(folderNames(folder_logic==1));
        % Only one write folder 
        write_folder = write_folder(1);
    end

    % Open protocol.json
    try 
        fid = fopen(append(write_folder,'/protocol.json'));
    catch
        error(append('Error: Could not open protocol.json file from unzipped folder. Check filename or write path. Current write path: ', write_folder))
    end

    % Read protocol.json
    try 
        raw = fread(fid);
        str = char(raw');
        % Create protocol_json structure
        protocol_json = jsondecode(str);
        fclose(fid);
    catch
        error('Error: Could not read protocol.json file. Check json formatting.')
    end

    % Check for headset settings in protocol_json
    try 
        headset = protocol_json.headset;
    catch
        % Give warning message if headset is defined in protocol.json
        warning('No headset defined in protocol.json. VicFirth headset will be used as default')
    end

    % Check for tablet settings in protocol_json
    try 
        tablet = protocol_json.tablet;
    catch
        % Give warning message if headset is defined in protocol.json
        warning('No tablet defined in protocol.json. TabE tablet will be used as default')
    end

    % Set hardware spec to match with .mat files in the hardware_spec_files folder
    if strcmp(headset, 'Creare Headset') || strcmp(headset,'WAHTS')
        hardware_spec_file = 'TabSINT-Creare_Headset-audio_profile.mat';
    else
        hardware_spec_file = strrep(append(tablet, '-', headset, '-audio_profile.mat'), ' ', '_');
    end

    % Check for calibration field in protocol.json
    if isfield(protocol_json, 'calibration')  
        % Convert .wav file names from cells to strings
        protocol_json.calibration.wavfiles = string(protocol_json.calibration.wavfiles);
        % Create cumulative_mat structure to save meta data of calibration
        cumulative_mat = struct(protocol_json.calibration);
        cumulative_mat.tablet = tablet;
        cumulative_mat.headset = headset;

        % Check if a reference file was defined
        if isfield(protocol_json.calibration, 'referenceFile')
            ref_name = protocol_json.calibration.referenceFile;
        else
            ref_name = '';
        end
        % Check if a reference level was defined
        if isfield(protocol_json.calibration, 'referenceLevel')
            ref_level = protocol_json.calibration.referenceLevel;
        else
            ref_level = [];
        end
        % Check if a calibration filter was defined
        if isfield(protocol_json.calibration, 'calibrationFilter')
            cal_filter = protocol_json.calibration.calibrationFilter;
        else
            % Set default filter to full
            cal_filter = 'full';
        end

        % Send protocol_json through the calibrate_files function
        [cumulative_mat] = calibrate_files(protocol_json, write_folder,...
            cumulative_mat, hardware_spec_file, cal_filter, ref_name,...
            ref_level,headset);
    else
        error('Error: Could not extract calibration block from protocol.json. Check that calibration block is defined and formatted correctly.')
    end
end
