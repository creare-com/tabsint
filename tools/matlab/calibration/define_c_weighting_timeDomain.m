function [b,a,c_w_discrete]=define_c_weighting_timeDomain(fs);
%function [b,a]=define_a_weighting_ztransform(fs);
%Created: Matt Swanson, August 16, 2023
%Purpose: Define the time-domain filter constants for a "C"
%         weighting of audio.  Output of this filter
%         is for use with the filter command.
%References: from https://www.cross-spectrum.com/audio/weighting.html I obtained
%         the following transfer function that supposedly defines
%         the C-Weighting filter...
%
%                 (4*pi*12200^2)*s^2
%         ----------------------------------
%         (s+2*pi*20.6)^2 * (s+2*pi*12200)^2
%


% %Define transfer function  
% num=poly([0 0]);
% den1=poly(-129.4);
% den2=poly(-76655);
% den=conv(conv(conv(den1,den1),den2),den2);
% c_weight=tf(num,den);
% 
% %Scale transfer function to yield 0dB response at 1000 Hz
% offset = 10*log10(abs(freqresp(c_weight,1000*2*pi)));
% num=(10^(0.1*(-offset)))*poly([0 0]);
% c_weight=tf(num,den);
% 
% %Convert from transfer function to discrete time system
% c_w_discrete = c2d(c_weight,1/fs,'matched');
% 
% %get filter parameters from the coeffients of the numerator
% %and denominator of the discrete transfer function
% b=c_w_discrete.num{1};
% a=c_w_discrete.den{1};

% Above is code to calculate filter coefficients using control toolbox
b = [0,0.319407515304259,-0.319407515304259,-0.319407515304259,0.319407515304259];
a = [1,-2.399630266026562,1.843480643461315,-0.484634475798000,0.040788708023834];
