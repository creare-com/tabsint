classdef calibrated_wav_file
    % calibrated_wav_file defines a singular wav file.
    % Constructor reads a *.wav file at arbitrary sample rate, resamples to
    % the standard rate, normalizes to [-1 1], and calculates weighted levels.
    %
    % From skilled_hearing_calibration.py - MCR 2014
    % Conversion to MATLAB - MSwanson 1/13/2023


    properties
        % Original file data
        file_name = [];         % File name (string)
        Fs = 48000;             % Samplerate of all final sound files
        Fs_raw = [];            % Samplerate extracted from original file
        wav_dynamic_range = []; % The maximum amplitude from the .wav
        wav = [];               % 48 kHz, DC bias removed, normed to [-1 1]
        cal_wav = [];           % Calibrated .wav data
        meta = struct();        % Meta data put into culumative_mat
    end
    
    methods
        function obj = calibrated_wav_file(file_name,write_folder,i)
            % Read .wav file, resample wav data to Fs sample rate, subtract
            % DC bias, normalize to [-1 1], calculate RMS with A & C filters
            % and without
            %
            % Arguments:
            % file_name: (string) Name of .wav file to be calibrated
            % write_folder: (string) Path to folder that holds uncalibrated files
            
            % Make path to .wav file
            file_path = string(append(write_folder,'/',file_name));
            
            % Save file name data and initialize meta data structure
            obj.file_name = file_name;

            % Make a meta data struct with the file name for each file
            % For the calibration.json file
            obj.meta(i) = struct();
            
            % Try to read .wav file
            try
                [wav_raw,obj.Fs_raw] = audioread(file_path);
            catch
                error(append('Error: Failed to read .wav file: ',file_name));
            end
             
            try 
                % Get the audio file bit information
                audio_info = audioinfo(file_path);
                bit_depth = audio_info.BitsPerSample;
                obj.wav_dynamic_range = (2.^bit_depth)./2 -1;
    
                % Convert the audio data to integer data type
                % based on the bit depth
                wav_raw = int16(wav_raw * (2^(bit_depth - 1) - 1));
            catch
                error(append('Error: Failed to get bit information for file: ',file_name));
            end

            %Put back to float
            wav_raw = double(wav_raw);

            % Resample to standard rate 
            wav_resampled = resample(wav_raw,obj.Fs,obj.Fs_raw);

            % Check if max value is 0
            if obj.wav_dynamic_range == 0
                error(append('Error: Cannot Calibrate zero-valued file ',file_name))
            end

            % Subtract DC bias and normalize to amplitude [-1 1]
            DC_bias = mean(wav_resampled);
            obj.wav = (wav_resampled - DC_bias)./obj.wav_dynamic_range;

            % Define A and C weighted filter
            [b,a]=define_a_weighting_timeDomain(obj.Fs);
            wav_A = filter(b,a,obj.wav(:,1));
            [b,a]=define_c_weighting_timeDomain(obj.Fs);
            wav_C = filter(b,a,obj.wav(:,1)); 

            % Calculate RMS
            obj.meta(i).RMSA = rms(wav_A(:,1));
            obj.meta(i).RMSC = rms(wav_C(:,1));
            obj.meta(i).RMSZ = rms(obj.wav(:,1));

        end
        
        function obj = calibrate(obj,file_name,hardware_spec,cal_filter,i)
            % Calibrate file for specific hardware. Populates cal_wav and 
            % associated meta data.
            %
            % Arguments:
            % file_name: (string) name of .wav file to be calibrated
            % hardware_spec: (struct) data stored in a struct with fields:
            %   'scaleFactor' is RMS ouput for a 1 kHz full scale input (Pa^-1)
            %   'b_fir' is an array of filter coefficients (designed for 48 kHz).
            % cal_filter: (string) 'full' or 'flat', i.e. use the filter or don't
            
            % Get the name of the meta data field for this file

            % Put scale factor and calibration filter into meta data
            obj.meta(i).scaleFactor = hardware_spec.scaleFactor;
            obj.meta(i).calibrationFilter = cal_filter;
        
            % Check to use filter or not, default uses filter i.e. 'full'
            if strcmp(cal_filter, 'full')
                % Apply hardware spec filter to .wav data
                filt_wav = filter(hardware_spec.b_fir, 1, obj.wav);
            elseif strcmp(cal_filter, 'flat')
                % No filter applied
                filt_wav = obj.wav;
            else
                error('Error: Invalid calibration filter defined (choose "full" or "flat")');
            end
        
            % Variable to keep track of all scaling and normalization factors
            cum_norm_factor = 1.0;
        
            % Calculate real_world_volume 
            if strcmp(obj.meta(i).refType, 'as-recorded')
                real_world_volume = obj.meta(i).realWorldRMSZ ...
                    .* obj.meta(i).scaleFactor ...
                    ./ obj.meta(i).RMSZ;
                % For as-recorded files, clip if necessary to achieve required volume.
                if real_world_volume > 1.0
                    as_rec_clip_scale = real_world_volume;
                    filt_wav = filt_wav .* as_rec_clip_scale;
                    % Set amplitude greater than dynamic range to 1 or -1
                    filt_wav(filt_wav > 1.0) = 1.0;
                    filt_wav(filt_wav < -1.0) = -1.0;
                    warning(append(file_name,' was clipped during as-recorded calibration'))
                else
                    as_rec_clip_scale = 1.0;
                end

                % Adjust normalization factor if clipped
                cum_norm_factor = cum_norm_factor * as_rec_clip_scale;
            end
            
            % Normalize to fill dynamic range of headset
            fill_factor = 1.0./max(abs(filt_wav(:,1)));

            % if channel is silent change fill factor to 0
            if fill_factor(1) || fill_factor(2) == inf
                inf_index = fill_factor == Inf;
                fill_factor(inf_index) = 0;
            end

            if fill_factor ~= 1.0
                % Apply fill factor to filtered wav data
                obj.cal_wav = filt_wav .* fill_factor;
                % Update scaling factor
                cum_norm_factor = cum_norm_factor .* fill_factor;
            else
                obj.cal_wav = filt_wav;
            end
            
            % Store cumulative normalization factor
            obj.meta(i).normFactor = cum_norm_factor;
            
            % To maintain backward compatibility
            obj.meta(i).wavRMSA = obj.meta(i).RMSA .* obj.meta(i).normFactor;
            obj.meta(i).wavRMSC = obj.meta(i).RMSC .* obj.meta(i).normFactor;
            obj.meta(i).wavRMSZ = obj.meta(i).RMSZ .* obj.meta(i).normFactor;
        end
    end
end


