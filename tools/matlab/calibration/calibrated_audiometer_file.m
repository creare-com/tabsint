classdef calibrated_audiometer_file
    % calibrated_audiometer_file
    % val 6/8/17 python class
    % Conversion to MATLAB - MSwanson 1/13/2023

    % Makes an Audiometer class instance that will record meta data and
    % read a .wav file. Does not calibrate.

    properties
        Fs = [];        % Samplerate of all final sound files
        cal_wav = [];   % Calibrated .wav data
        meta = [];      % Meta data put into culumative_mat
    end
    
    methods
        function obj = calibrated_audiometer_file(file_name,write_folder)
            % Initializes class and reads .wav file
            %
            % Arguments:
            % file_name: (string) name of .wav file to be read
            % write_folder: (string) Path to folder that holds uncalibrated files

            % Make path to .wav file
            file_path = string(append(write_folder,'/',file_name));
            
            % Save file name data and initialize meta data structure
            obj.file_name = file_name;
            obj.meta = struct();
            obj.meta.file = file_name;
            try
                [obj.cal_wav, obj.Fs] = audioread(file_path);
            catch
                error(append('Error: Failed to read audiometer file ',file_name));
            end
        end
    end
end
