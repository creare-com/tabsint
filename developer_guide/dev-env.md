# Development Environment

## WSL / Linux

To build and develop the TabSINT software, you must set up your local machine with the appropriate developer dependencies.

All the following commands should be run on your WSL machine unless specified otherwise. 

### Install [Git](https://git-scm.com/)

### Install NVM and NodeJS

```bash
sudo apt install curl 
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
source ~/.profile 
nvm -v
nvm install node 
nvm install 14.18.1
node -v
nvm use 14.18.1
sudo apt-get install openjdk-11-jdk
java -version
```
NVM is not only good for this tabSINT project, but for managing multiple project node versions and switching with ease. 

### Install ADB 

**Versions need to match from WSL to Windows in order for this to work properly; version currently used 1.0.41**

```bash
mkdir -p /usr/local/android-sdk
cd /usr/local/android-sdk/
curl -OL https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip platform-tools-latest-linux.zip 
rm -f platform-tools-latest-linux.zip 
ln -s /usr/local/android-sdk/platform-tools/adb /usr/bin/adb  # it's okay if this one doesn't work
export PATH=/usr/local/android-sdk/platform-tools:${PATH} 
echo "export PATH=/usr/local/android-sdk/platform-tools:${PATH}"
adb version

```
The ADB platform-tools should be installed on Windows as well. Use the Android Studio Steps in the Windows -> Android section of this document.
Once Android is installed properly on windows, run the following commands in a Windows Terminal: 
```bash
adb kill-server
adb -a nodaemon server
```  
Then run the following commands on WSL: 
```bash
export ADB_SERVER_SOCKET=tcp:$(tail -1 /etc/resolv.conf | cut -d' ' -f2):5037
adb devices
```

If you have a device connected it should show up in the list of devices. The export command can be added to your .bashrc file is needed or you can run it when running WSL.

### Repositories
Make sure you have both the tabsint and the cordova-plugin-creare-cha repositories on your machine. 
Setup a `tabsint.json` file in the config folder in tabsint similar to the following: 
```json
{

    "build": "tabsint",
  
    "description": "Official tabsint release for android",
  
    "cordovaPlugins": [
  
      {
  
        "package": "com.creare.cordova.cha",
  
        "src": "/home/<username>/repos/cordova-plugin-creare-cha/"
  
      }
  
    ]
  
}
```
Change the `src` to work with your file path.

### Install Android build-tools

1. Go here: https://developer.android.com/studio
2. Scroll down to `Command line tools only`
3. Download the linux version
4. Move the zip file into your wsl home directory
5. Make a directory called `android-tools`. (If you need sudo, edit your permissions using chmod. rwe permissions will be needed for building tabsint.)
6. Move the zip file into the `android-tools` directory
7. Make a `build-tools` directory inside the `android-tools` directory

```bash
sudo unzip commandlinetools-linux-9477386_latest.zip
sudo rm commandlinetools-linux-9477386_latest.zip
cd cmdline-tools/bin
sudo bash sdkmanager --install "platform-tools" "platforms;android-32" "build-tools;32.0.0" --sdk_root=.
sudo bash sdkmanager --licenses --sdk_root=.
mv ~/android-tools/cmdline-tools/bin/build-tools/32.0.0 ~/android-tools/build-tools/32.0.0
cp -R ~/android-tools/cmdline-tools/bin/licenses ~/android-tools/
```
### Add exports to the end of your .bashrc file with `sudo nano ~/.bashrc`; make sure to change the paths accordingly

```bash
export ANDROID_HOME="/home/<username>/android-tools"
export ANDROID_SDK_ROOT="/home/<username>/android-tools"
export PLATFORM_TOOLS="/usr/local/android-sdk/platform-tools"
export JAVA_HOME="/usr/lib/jvm/java-1.11.0-openjdk-amd64/"
export PATH="$ANDROID_HOME/cmdline-tools:$ANDROID_HOME/cmdline-tools/bin:$PLATFORM_TOOLS:$JAVA_HOME:$PATH"
```
### Gradle

Install Gradle using 'apt install gradle'

## Windows

To build and develop the TabSINT software, you must set up your local machine with the appropriate developer dependencies.

Confirm you have the following tools:

- [Git](https://git-scm.com/)
- [Node](https://nodejs.org/)
  - Make sure global `node_modules` directory is on your system path
  - **Node** comes with a command line package manager `npm`
  - Currently requires Node 14.x (14.18.1 as of Tabsint v4.5.0)

**NOTE: When installing Git for Windows, I had to add a couple extra things to my path, per this [stackoverflow post](https://stackoverflow.com/a/50833818). When installing Node, select the Current version. Do not install the LTS version.**

## Setting up TabSINT and its Dependencies

Once you have the required tools, clone this git repository somewhere on your local machine and enter the `tabsint` directory. If you are uncomfortable using git from the command line, we recommend [SourceTree](https://www.sourcetreeapp.com/).

From the `tabsint` directory, run the following command to install dependencies (npm modules and bower components):

```bash
$ npm install
```

At this point, you are ready to serve the app locally in the browser for testing.

### Tablet Dependencies

A few extra tools are necessary to build the app for mobile devices:

#### Android

To build an android package you need the following tools:

- [JAVA JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
  - Currently requires Java JDK 11.0.12
  - Confirm that you have the environment variable `JAVA_HOME` set to be the root of the JDK directory (i.e. `C:\Program Files\Java\jdkx.x.x`)
  - **NOTE: Oracle's download page seems broken (October 26, 2020), so I used [this](https://adoptopenjdk.net/?variant=openjdk8&jvmVariant=hotspot) to install the JDK**
- [Android Studio](https://developer.android.com/sdk/index.html)
  - Note down the path where the Android SDK is installed
  - Make sure the following directories within the Android SDK root directory are on your system path:
    - `[path-to-sdk]/tools/`
    - `[path-to-sdk]/platform-tools/`
    - `[path-to-sdk]/build-tools/[sdk-version]/` where `[sdk-version]` is the SDK version you have installed (32 as of TabsINT v4.5.0).
    - **NOTE: if using the Android Studio (Windows) sdkmanager to install the tools, platform-tools and build-tools, the installed files might be found in the C:\Users\User\AppData\Local\Android\Sdk directory. To fix this issue I copied these folders to my C:\Program Files\Android\Android Studio directory.**
- [Gradle](https://gradle.org/install/)
  - Current version on continuous integration jobs is 7.3.3.
  - Note down the path where Gradle is installed
  - Make sure the bin directory within your Gradle installation is on your system path.
- As of Android 12, `ANDROID_HOME` is now the preferred environment variable, and not `ANDROID_SDK_ROOT`. Depending on your environment, you may need to define these.

#### iOS

To build an iOS package, you must be developing on a mac with the current build of XCode. Additionally, you should ensure that you have the XCode command-line-tools. Get them by entering the following into a Terminal:

```bash
$ xcode-select --install
```

Cordova may have additional requirements. To find these and how to satisfy them, type the following into the Terminal:

```bash
$ npm run cordova -- requirements
```
