# Protocol Development Tools

The [TabSINT user guide](https://tabsint.org/docs/user-guide/protocols.html) goes into great depth about writing and developing protocols.
The source code has many features to help you debug and refine your protocols.

This document will describe more advanced tools to help in the protocol development process. 

## Validating a Protocol

The npm scripts have a built in validator to check the your protocol conforms to the master schema. To validate a protocol, run:

``` bash
$ npm run protocol-directory   # shorthand if protocol directory is in `www/protocols`
$ npm run validate path/to/protocol-directory
```

If the input is not a path (i.e. `mockProtocol`), the validator will look in the `www/protocols/` directory.

If the input is a path (i.e. `other-directory/mock`), the validator will look for the protocol inside that directory.
All input paths must be relative to the tabsint root directory.

## Testing a Protocol in the Browser

Serving the app provides a great way to test and iterate protocols quickly.
As described in [Building and Running](building-running.md), you can serve the app in the browser using the command: `npm run serve`
This server will update every time a change is in the source code.

#### Load the Protocol in the App

Create a new directory in the `src/` directory of the cloned tabsint repository titled `protocols`. Inside this new directory, make a new directory named for your protocol. Place the protocol files in this directory, including your `protocol.json`. 

Now, in your config file (`/src/tabsintConfig.json`) include a new key named `protocols` and set the value to be an array of all the protocol directory names you want to load. For instance, if your protocol is in a directory `myProtocol`, you would write into your config file:

```
{
  "build": "tabsint",
  "description": "Official tabsint release for android",
  "platform": "android",
  "protocols": [
    "myProtocol"
  ],
  ...
```

Now that your protocol is available to the source code, open TabSINT in the browser, ensure that you are in `AdminMode`, and navigate to the *Protocols* section of the **Admin Page**. You should see your protocol listed in the table. Select your protocol in this pane, then click `Load`. You should now be able to switch to Exam View and begin testing your protocol.

#### Developer Tools

In Chrome, press `F12` (windows) or `Option+Command+J` (mac) to open the developer tools. There, you can:

- View any errors in the **Console**
- Set breakpoints in the **Source Code** to explore errors
- Set the **Emulate** tab to your tablet to see the protocol as it will appear on the tablet

## Custom Response Areas

Advanced users can include custom response areas that extend the standard TabSINT functionality.

These pages can be used for an additional type of response area or to analyze and display results. Below is the list of requirements and an example use case.

> **Warning:** These response areas are difficult to write and debug properly. 
> Creare can develop custom response areas, which can then be extended or customized. 
> This functionality is exposed for *advanced users only*.
> Effective protocol development requires, at a minimum, familiarity with:
> 
> - Angularjs
> - TabSINT
> - The [Lodash Javascript Library](https://lodash.com/docs)
> - Bootstrap and AngularUI

### Requirements ###

- The javascript file and html files explained below must be included in the zip file with the protocol.
- Javascript File(s):
	- The javascript file(s) must be defined as a string or an array in the field "js", at the top level of the protocol
	- The module must be wrapped in a ```(function() { // content })()``` statement
	- The module can contain one or more angular controllers used in the htmls files attached to custom response areas. These controllers are registered using the `tabsint.controller` service, and must be referenced in the custom html files using ng-controller="controllerName"
	- Note the available variables and methods attached to $scope, commented below.  Only variables on $scope (i.e. $scope.score) are available in the html of custom response areas!
	- Sample javascript file ```customPageControllers.js```

```
(function() {

  tabsint.controller('SummaryResultsCtrl', function ($scope) {

    // Read-Only $scope variables:
    //  $scope.responses :
    //      description : results
    //      fields: response, correct, eachCorrect, numberCorrect, numberIncorrect
    //  $scope.tabletLocation :
    //      description : gps coords
    //      fields : latitude, longitude
    //  $scope.site :
    //      description : tablet info
    //      fields : siteName, siteId, protocolId, protocolHash, protocolName, protocolCreationDate, protocolOwner

    // Read/Write $scope variables:
    //  $scope.page : 
    //      description : points you to examLogic.dm.page;
    //      you can edit properties of the page here, including the presented title, question test, submittable logic, etc
    //  $scope.flags :
    //      description : points to examLogic.dm.state.flags;
    //      you can set or check these, but be careful - Changing flags mid-exam may lead to unpredicted behavior!

    // custom functionality (example)
    $scope.score = ($scope.nCorrect - $scope.nIncorrect)/$scope.nResponses;
  });

  tabsint.controller('CustomPageCtrl', function ($scope) {

    // Another controller, tied to a different custom page and html, could go here.
    $scope.displayText = 'This is a Custom Page';

  });

})();
```

- HTML Files:
	- One HTML file is required for each custom page type. Start the HTML by assigning a custom controller from the custom javascript file.
	- Sample HTML files `summaryResults.html` and `customPage.html`

{% raw %}
```
	<div ng-controller="SummaryResultsCtrl">
		<row>Protocol Name : {{site.protocolName}}</row>
		<row>Score : {{score}}</row>
	</div>
```
{% endraw %}

{% raw %}
```
	<div ng-controller="CustomPageCtrl">
        	<row>Protocol Name : {{site.protocolName}}</row>
        	<row>{{displayText}}</row>
	</div>
```
{% endraw %}

- Protocol Block:
	- The syntax for adding multiple ```customResponseArea``` pages in the ```protocol.json``` is shown below.

```
 	{
            "js": "customPageControllers.js", // the same javascript file for ALL custom page controllers
            "pages":[
                ...,
                {
                    "id":"summaryResults1",
                    "title":"Summary Results",
                    "questionMainText":"The Results of the Last Five Questions",
                    "responseArea": {
                        "type": "customResponseArea",
                        "html": "summaryResults.html"    // the html file specific to this custom page type
                    },
                    "submitText": "Custom Submit Button Text"
                },
                {
                    "id":"customPage1",
                    "title":"Custom Page",
                    "questionMainText":"This is a Second Custom Page",
                    "responseArea": {
                        "type": "customResponseArea",
                        "html": "customPage.html"    // the html file specific to this custom page type
                    },
                    "submitText": "Custom Submit Button Text"
                },
                ...
             ]
         }
```

### Additional Functionality - Save To File 

TabSINT custom response area functionality can be expanded to include the ability to save intermediate data to a file.

#### Implementation

1. Dependency-inject the library `tabsintFS` into your `customResponseArea` controller. 
2. Call ```tabsintFS.createFile(disk.extStorageRootDir, filenameWithPath, "mime type").then(uri => {return tabsintFS.writeStringFile(uri, angular.toJson(ret));}))```. 

OR 

Call ```tabsintFS.createFile(disk.extStorageRootDir, filenameWithPath, "mime type").then(uri => {return tabsintFS.writeBytesFile(uri, blob);})```. 

#### Example of Saving Data to File

See:
* log-exports.js
* results.js
* pdf.js


> **Note:** While the file will write immediately, the file system may not update to show the new file from a computer until you restart the tablet.  
> The file will show up immediately on the tablet and using more advanced file browsers on the computer side.

## Communicate with External Apps 

For additional feature flexibility, TabSINT protocol pages can interface with other apps installed on the tablet or smart phone.

In an `externalAppResponseArea`, TabSINT switches focus to another specified app, optionally sending data as well.  Then TabSINT waits for the other app to return focus to TabSINT.

The other app can perform any desired function, then pass focus and data back to TabSINT.  That data is stored as the response for the externalApp page, and the protocol continues.

### External App Requirements

For the externalApp feature to work properly, the other app must be able to properly interface with TabSINT.

See [tabsint-crosstalk](https://gitlab.com/creare-com/tabsint-crosstalk) app for a working example.

The example app depends on the [TabSINT Receiver plugin](https://gitlab.com/creare-com/cordova-plugin-creare-tabsintreceiver).

### Options ###

* `appName` - must match the package name of another installed
* `data` - object that can be passed to external app on initialization

### Protocol Example ###
```
{
  "id": "externalApp1",
  "title": "External App",
  "questionMainText":"This page will launch another app to perform tasks external to TabSINT.",
  "questionSubText":"When the external app is done performing these tasks, focus will return to TabSINT.",
  "responseArea":{
    "type":"externalAppResponseArea",
    "appName": "com.creare.crosstalk",
    "dataOut": {
      "message": "Run-Exam-1",
      "data": 10
    }
  }
}
```

## MATLAB for Protocol Development ##

The jsonlab toolbox for Matlab can be downloaded [here](http://iso2mesh.sourceforge.net/cgi-bin/index.cgi?jsonlab).  

Matlab is useful when a large group of similar questions need to be included in a protocol.  Write the protocol with one question, load into Matlab, copy the relevant question, substitute appropriate values for the question and responses, and then write out as JSON.

## Converting a calibrated streaming protocol to a WAHTS native protocol
#### Goal
Create a procedure to modify a protocol that currently uses a connected  VicFirth headset (with calibration) to play files, and instead use the “almost” identical protocol to play files from the SD card at the same target levels. 

#### First, write the media on the WAHTS SD card. [Example](https://gitlab.com/creare-com/tabsint/blob/master/tools/matlab/write_media.m)

  - Rescale and resample the media. 
  - Rename files according the WAHTS naming convention  which follows the [FAT 8.3 naming scheme](https://en.wikipedia.org/wiki/8.3_filename).

		* Limited to at most eight characters 
	    * Legal characters:
			- Upper case letters A–Z
			- Numbers 0–9
			- Space (though trailing spaces in either the base name or the extension are considered to be padding and not a part of the filename, also filenames with spaces in them must be enclosed in quotes to be used on a DOS command line, and if the DOS command is built programmatically, the filename must be enclosed in quadruple quotes when viewed as a variable within the program building the DOS command.)
			- ! # $ % & ' ( ) - @ ^ _ ` { } ~
		
		* Illegal characters:
			- " * + , / : ; < = > ? \ [ ] | 
  - [Put the media on the WAHTS](http://tabsint.org/docs/user-guide/cha.html#loading-audio-files)
  

#### Second, update `protocol.json`.

  - Replace `headset` parameter from `VicFirth` to `WAHTS`.
  - Delete `calibration` array.
  - Replace `wavfiles` array on each page with `chaWavFiles`.
  - Replace `wavfile` properties with `chaWavFiles` properties.
	  * `path` stays the same. Ensure it is the correct path that was writen to the WAHTS SD card.
	  * Set `UseMetaRMS` to true.
	  * Specify `Leq`. Default is [72, 72]. 
